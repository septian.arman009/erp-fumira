<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marketing extends Model
{
    
    protected $fillable = ['do_number', 'product_id', 'qty', 'date_request', 'date_send', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function ppic()
    {
        return $this->hasMany('App\Ppic');
    }

    public function fgs()
    {
        return $this->hasMany('App\Fgs');
    }

}
