<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncDetail extends Model
{
    protected $fillable = ['incoming_id', 'raw_pcn', 'raw_id', 'length', 'status'];

    public function incoming()
    {
        return $this->belongsTo('App\Incoming');
    }

    public function raw()
    {
        return $this->belongsTo('App\Raw');
    }

    public function spkdetail()
    {
        return $this->hasOne('App\SpkDetail');
    }

    public function colored()
    {
        return $this->hasMany('App\Colored');
    }

}
