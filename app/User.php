<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function generateVerificationToken()
    {
        $token = $this->verification_token;
        if(!$token){
            $token = str_random(40);
            $this->verification_token = $token;
            $this->save();
        }
        return $token;
    }

    public function sendVerification()
    {
        $token = $this->generateVerificationToken();
        $user = $this;
        Mail::send('auth.emails.verification', compact('user', 'token'), function ($m) use ($user){
            $m->to($user->email, $user->name)->subject('Obat Skripsi verification email');
        });
    }

    public function verify()
    {
        $this->email_verified_at = date('Y-m-d H:i:s');
        $this->is_verified = 1;
        $this->verification_token  = null;
        $this->save();
    }

    public function color()
    {
        return $this->hasMany('App\Color');
    }

    public function group()
    {
        return $this->hasMany('App\Group');
    }

    public function incoming()
    {
        return $this->hasMany('App\Incoming');
    }

    
    public function marketing()
    {
        return $this->hasMany('App\Marketing');
    }
    
    public function ppic()
    {
        return $this->hasMany('App\Ppic');
    }

    public function product()
    {
        return $this->hasMany('App\Product');
    }

    public function raw()
    {
        return $this->hasMany('App\Raw');
    }

}
