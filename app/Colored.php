<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colored extends Model
{
    protected $fillable = ['inc_detail_id', 'colored_pcn', 'color_id', 'length', 'status'];

    public function incdetail()
    {
        return $this->belongsTo('App\IncDetail');
    }

    public function spkcoloreddetail()
    {
        return $this->hasOne('App\SpkColoredDetail');
    }

    public function color()
    {
        return $this->belongsTo('App\Color');
    }
}
