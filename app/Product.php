<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = ['product_type', 'type', 'cap', 'thick', 'width', 'length', 'color_id', 'user_id', 'gramatur'];

    public function marketing()
    {
        return $this->hasMany('App\Marketing');
    }

    public function color()
    {
        return $this->belongsTo('App\Color');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
