<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Laminating extends Model
{
    protected $fillable = [
        'marketing_id',
        'pcn',
        'lot',
        'length',
        'user_id',
        'color_type',
        'description',
        'status'
    ];

    public function marketing()
    {
        return $this->belongsTo('App\Marketing');
    }
}
