<?php

function generateId($id, $prefix)
{
    if ($id < 10) {
        $id = '0000' . $id;
    } else if ($id >= 10 && $id < 100) {
        $id = '000' . $id;
    } else if ($id >= 100 && $id < 1000) {
        $id = '00' . $id;
    } else if ($id >= 1000 && $id <= 10000) {
        $id = '0' . $id;
    }

    return $prefix .''. $id;
}

function lot($lot)
{
    if ($lot < 10) {
        $lot = '00' . $lot;
    } else if ($lot >= 10 && $lot < 100) {
        $lot = '0' . $lot;
    } else if ($lot >= 100 && $lot < 1000) {
        $lot = $lot;
    } 

    return $lot;
}

function indoFullDateDay($datetime)
{
    $datetime = explode(' ', $datetime);
    $date = explode('-', $datetime[0]);

  

    if($date[1] == 1){
        $bulan = 'Januari';
    }else if($date[1] == 2){
        $bulan = 'Februari';
    }else if($date[1] == 3){
        $bulan = 'Maret';
    }else if($date[1] == 4){
        $bulan = 'April';
    }else if($date[1] == 5){
        $bulan = 'Mei';
    }else if($date[1] == 6){
        $bulan = 'Juni';
    }else if($date[1] == 7){
        $bulan = 'Juli';
    }else if($date[1] == 8){
        $bulan = 'Agustus';
    }else if($date[1] == 9){
        $bulan = 'September';
    }else if($date[1] == 10){
        $bulan = 'Oktober';
    }else if($date[1] == 11){
        $bulan = 'November';
    }else if($date[1] == 12){
        $bulan = 'Desember';
    }

    return $date[2].' '.$bulan.' '.$date[0];

}
