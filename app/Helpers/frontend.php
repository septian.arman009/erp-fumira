<?php

Html::macro('mainMenu', function ($segment, $url, $fa, $title) {
    $class = (in_array($segment, request()->segments())) ? 'active' : '';
    return "<li class=\"$class\"><a href=\"$url\"><span class=\"$fa\"></span> <span class='xn-text'>$title</span></a></li>";
});

Html::macro('subMenu', function ($segment, $url, $fa, $title) {
    $class = (in_array($segment, request()->segments())) ? 'active' : '';
    return "<li class=\"$class\"><a href=\"$url\"><span class=\"$fa\"></span>$title</a> </li>";
});

function set_active($segment)
{
    return $class = (in_array($segment, request()->segments())) ? 'active' : '';
}
