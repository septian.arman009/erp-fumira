<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incoming extends Model
{
    protected $fillable = ['inc_number', 'user_id', 'status'];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function incdetail()
    {
        return $this->hasMany('App\IncDetail');
    }
}
