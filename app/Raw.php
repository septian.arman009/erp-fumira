<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Raw extends Model
{
    protected $guarded = ['id'];
    protected $fillable = ['raw_type', 'spec', 'thick', 'width', 'user_id'];

    public function incdetail()
    {
        return $this->hasMany('App\IncDetail');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
}
