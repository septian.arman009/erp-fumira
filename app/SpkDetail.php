<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpkDetail extends Model
{

    protected $fillable = ['ppic_id', 'inc_detail_id'];

    public function ppic()
    {
        return $this->belongsTo('App\Ppic');
    }

    public function incdetail()
    {
        return $this->belongsTo('App\IncDetail');
    }
}
