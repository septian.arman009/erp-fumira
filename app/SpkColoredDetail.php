<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpkColoredDetail extends Model
{
    protected $fillable = ['ppic_id', 'colored_id'];

    public function ppic()
    {
        return $this->belongsTo('App\Ppic');
    }

    public function colored()
    {
        return $this->belongsTo('App\Colored');
    }
}
