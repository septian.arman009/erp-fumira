<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UserShouldVerified
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if(Auth::check() && !Auth::user()->is_verified) {
            $link = url('auth/send-verification').'?email='.urlencode(Auth::user()->email);
            Auth::logout();

            Session::flash("flash_notification",[
                "level" => "warning",
                "message" => "Your account isn't active, please activate by click the link that we've sent to your email.
                <a class='alert-link' href='$link'>Resent Email Verification</a>"
            ]);

            return redirect('/login');
        }

        return $response;
    }
}
