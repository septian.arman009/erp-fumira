<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Session;
use Auth;

class RegisterController extends Controller
{

    use RegistersUsers;

    protected $redirectTo = 'admin/home';

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
    
    public function verify(Request $request, $token) { 
        $email = $request->get('email');
        $user = User::where('verification_token', $token)->where('email', $email)->first();
        if($user){
            $user->verify();
            Session::flash("flash_notification", [
                "level" => "success",
                "message" => "Email verification successfull"
            ]);
            Auth::login($user);
        }
        return redirect('admin/home');
    }

    public function sendVerification(Request $request)
    {
        $user = User::where('email', $request->get('email'))->first();
        if($user && !$user->is_verified){
            $user->sendVerification();
            Session::flash("flash_notification", [
                "level" => "success",
                "message" => "Please click to the link that we've sent to your email"
            ]);
        }
        return redirect('/login');
    }
}
