<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Marketing;
use App\Product;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;
use Auth;
use DB;

class MarketingsController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $orders = Marketing::select(['id', 'do_number', 'product_id', 'qty', 'date_request', 'date_send', 'user_id'])->get();
            return Datatables::of($orders)
                ->addColumn('product_type', function ($order) {
                    return ucfirst($order->product->product_type);
                })
                ->addColumn('user', function ($order) {
                    return ucfirst($order->user->name);
                })
                ->addColumn('action', function ($order) {
                    return view('admin.datatables._action', [
                        'id' => $order->id,
                        'destroy_url' => route('orders.destroy', $order->id),
                        'edit_url' => route('orders.edit', $order->id),
                        'confirm_message' => 'Apa anda yakin ingin menghapus tipe warna ' . $order->do_number . ' ?',
                    ]);
                })
                ->rawColumns(['product_type', 'user', 'action'])
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('do_number'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['do_number'], $request->get('do_number')) ? true : false;
                        });
                    }
                    if (!empty($request->get('product_type'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['product_type'], $request->get('product_type')) ? true : false;
                        });
                    }
                    if (!empty($request->get('qty'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['qty'], $request->get('qty')) ? true : false;
                        });
                    }
                    if (!empty($request->get('date_request'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['date_request'], $request->get('date_request')) ? true : false;
                        });
                    }
                    if (!empty($request->get('date_send'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['date_send'], $request->get('date_send')) ? true : false;
                        });
                    }
                    if (!empty($request->get('user'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['user'], $request->get('user')) ? true : false;
                        });
                    }
                })
                ->make(true);
        }

        $data['title'] = 'Daftar Delivey Order (DO)';
        return view('admin.marketing.index', $data);
    }

    public function create()
    {
        $data['do_number'] = $this->getId('marketings');
        $data['title'] = 'Tambah Delivery Order (DO)';
        return view('admin.marketing.create', $data);
    }

    public function getId($table)
    {
        $last = DB::table($table)->latest()->first();
        
        if($last){
            return generateId($last->id + 1, 'DO/');
        }else{
            return $id = 'DO/00001';
        }
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'do_number' => 'required|unique:marketings',
            'product_id' => 'required',
            'qty' => 'required',
            'date_request' => 'required',
            'date_send' => 'required'
        ]);

        if ($validator->passes()) {
            $data = $request->all();
            $data['user_id'] = Auth::user()->id;
            $order = Marketing::create($data);
            return response()->json(['success' => 'Berhasil menambahkan Delivery Order (DO)']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $title = 'Edit Delivery Order (DO)';
        $order = Marketing::find($id);
        return view('admin.marketing.edit')->with(compact('order', 'title'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'do_number' => 'required|unique:marketings,do_number,'.$id,
            'product_id' => 'required',
            'qty' => 'required',
            'date_request' => 'required',
            'date_send' => 'required'
        ]);

        if ($validator->passes()) {
            $order = Marketing::find($id);
            $order->update($request->all());
            return response()->json(['success' => 'Berhasil mengubah data Delivery Order (DO)']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function destroy($id)
    {
        $order = Marketing::where('id', $id)->delete();
        return response()->json(['id' => $id]);
    }

    

    public function productDetail($id)
    {
        $product = Product::where('id', $id)->first();
        $data = [
            'product_type' => $product->product_type,
            'type' => $product->type,
            'cap' => $product->cap,
            'color_name' => ($product->color_id != null) ? $product->color->color_name : 'Tanpa Warna',
            'thick' => $product->thick,
            'width' => $product->width,
            'length' => $product->length
        ];

        return response()->json(['data' => $data]);
    }
}
