<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\IncDetail;
use App\Incoming;
use App\Raw;
use Auth;
use Datatables;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;

class InvController extends Controller
{
    public function incCreate()
    {
        $onproccess = Incoming::where([
            ['status', 'proccess'],
            ['user_id', Auth::user()->id],
        ])->first();

        if ($onproccess) {
            $data['inc_number'] = $onproccess->inc_number;
        } else {
            $data['inc_number'] = $this->getId('incomings');
        }

        $data['title'] = 'Terima Barang Masuk';
        return view('admin.inventory.incoming.create', $data);
    }

    public function getId($table)
    {
        $last = DB::table($table)->latest()->first();

        if ($last) {
            return generateId($last->id + 1, 'INC/');
        } else {
            return $id = 'INC/00001';
        }
    }

    public function incRawDetail($id)
    {
        $raw = Raw::where('id', $id)->first();
        $data = [
            'spec' => $raw->spec,
            'thick' => $raw->thick,
            'width' => $raw->width,
        ];

        return response()->json(['data' => $data]);
    }

    public function incStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'inc_number' => 'required',
            'raw_pcn' => 'required|unique:inc_details',
            'raw_id' => 'required',
            'length' => 'required',
        ]);

        if ($validator->passes()) {
            $check_inc = Incoming::where('inc_number', $request->inc_number)->first();
            if (is_null($check_inc)) {
                $incoming = Incoming::create([
                    'inc_number' => $request->inc_number,
                    'user_id' => Auth::user()->id,
                    'status' => 'proccess',
                ]);

                $inc_detail = IncDetail::create([
                    'incoming_id' => $incoming->id,
                    'raw_pcn' => $request->raw_pcn,
                    'raw_id' => $request->raw_id,
                    'length' => $request->length,
                    'status' => 'Ready',
                ]);
            } else {
                $inc_detail = IncDetail::create([
                    'incoming_id' => $check_inc->id,
                    'raw_pcn' => $request->raw_pcn,
                    'raw_id' => $request->raw_id,
                    'length' => $request->length,
                    'status' => 'Ready',
                ]);
            }

            return response()->json(['success' => 'Konfirmasi terima barang berhasil, stock Raw Material ditambahkan']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function incDetail(Request $request)
    {
        $incNumber = $request->inc_number;
        $incoming = Incoming::where('inc_number', $incNumber)->first();
        $data = array();

        if ($incoming) {
            $incDetail = IncDetail::where('incoming_id', $incoming->id)->get();
            $html = '';
            foreach ($incDetail as $detail) {
                $html .= '<tr>';
                $html .= '<td>' . $incNumber . '</td>';
                $html .= '<td>' . $detail->raw_pcn . '</td>';
                $html .= '<td>' . $detail->raw->thick . '</td>';
                $html .= '<td>' . $detail->raw->width . '</td>';
                $html .= '<td>' . $detail->length . '</td>';
                $html .= '
                    <td><a onclick="remove(' . $detail->id . ')" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> Hapus</a></td>
                ';
                $html .= '</tr>';

                $data[] = $html;
                $html = '';
            }
        }

        return response()->json(['html' => $data]);

    }

    public function incFinish(Request $request)
    {
        $incoming = Incoming::where('inc_number', $request->inc_number)->first();
        if ($incoming->incdetail) {
            $incoming->status = 'finish';
            $incoming->save();
            
            return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'empty']);
        }
    }

    public function rawStock(Request $request)
    {
        if ($request->ajax()) {
            $raws = IncDetail::select(['id', 'incoming_id', 'raw_pcn', 'raw_id', 'length', 'created_at'])->get();
            return Datatables::of($raws)
                ->addColumn('inc_number', function ($raw) {
                    return $raw->incoming->inc_number;
                })
                ->addColumn('raw_type', function ($raw) {
                    return $raw->raw->raw_type;
                })
                ->addColumn('thick', function ($raw) {
                    return $raw->raw->thick;
                })
                ->addColumn('width', function ($raw) {
                    return $raw->raw->width;
                })
                ->addColumn('user', function ($raw) {
                    return $raw->incoming->user->name;
                })
                ->addColumn('action', function ($raw) {
                    return view('admin.datatables._action', [
                        'id' => $raw->id,
                        'destroy_url' => route('stocks.destroy', $raw->id),
                        'edit_url' => route('stocks.edit', $raw->incoming->id),
                        'confirm_message' => 'Apakah anda yakin ingin menghapus stock material ' . $raw->raw_pcn . ' ?',
                    ]);
                })
                ->rawColumns(['inc_number', 'raw_type', 'thick', 'width', 'action'])
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('inc_number'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['inc_number'], $request->get('inc_number')) ? true : false;
                        });
                    }
                    if (!empty($request->get('raw_pcn'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['raw_pcn'], $request->get('raw_pcn')) ? true : false;
                        });
                    }
                    if (!empty($request->get('raw_type'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['raw_type'], $request->get('raw_type')) ? true : false;
                        });
                    }
                    if (!empty($request->get('thick'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['raw_pcn'], $request->get('raw_pcn')) ? true : false;
                        });
                    }
                    if (!empty($request->get('width'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['width'], $request->get('width')) ? true : false;
                        });
                    }
                    if (!empty($request->get('length'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['length'], $request->get('length')) ? true : false;
                        });
                    }
                    if (!empty($request->get('user'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['user'], $request->get('user')) ? true : false;
                        });
                    }
                })
                ->make(true);
        }

        $data['title'] = 'Stock Raw Material';
        return view('admin.inventory.raw_stock', $data);
    }

    public function editStock($id)
    {
        $title = 'Edit Barang Masuk';
        $inc_number = Incoming::where('id', $id)->first()->inc_number;
        return view('admin.inventory.incoming.edit')->with(compact('inc_number', 'title'));
    }

    public function destroyStock($id)
    {
        $raw = IncDetail::where('id', $id)->delete();
        return response()->json(['id' => $id]);
    }

    public function removeStock($id)
    {
        $raw = IncDetail::where('id', $id)->delete();
        return response()->json(['status' => 'success']);
    }
}
