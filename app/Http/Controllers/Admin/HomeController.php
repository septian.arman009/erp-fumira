<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Fgs;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($tahun = 2019)
    {
        $fgs = Fgs::whereYear('created_at', $tahun)->get();
        $coil = 0;
        $pelas = 0;
        $roll = 0;
        $forming = 0;
        $corugation = 0;
        $total = 0;
        $not = 0;
        foreach ($fgs as $fg) {
            if ($fg->marketing->product->type == 'Coil') {
                $coil += ($fg->length * $fg->marketing->product->gramatur)/1000;
            } else if ($fg->marketing->product->type == 'Pelas') {
                $pelas += ($fg->length * $fg->marketing->product->gramatur)/1000;
            } else if ($fg->marketing->product->type == 'Roll') {
                $roll += ($fg->length * $fg->marketing->product->gramatur)/1000;
            } else if ($fg->marketing->product->type == 'Forming') {
                $forming += ($fg->length * $fg->marketing->product->gramatur)/1000;
            } else if ($fg->marketing->product->type == 'Corugation') {
                $corugation += ($fg->length * $fg->marketing->product->gramatur)/1000;
            }

            if($fg->status == 'NOT'){
                $not += ($fg->length * $fg->marketing->product->gramatur)/1000;
            }

            $total += ($fg->length * $fg->marketing->product->gramatur)/1000;
        }

        $data['bar'] = [
            [
                'y' => 'Coil '. $coil. ' kg',
                'a' => $coil
            ],
            [
                'y' => 'Pelas '. $pelas. ' kg',
                'a' => $pelas
            ],
            [
                'y' => 'Roll '. $roll. ' kg',
                'a' => $roll
            ],
            [
                'y' => 'Forming '. $forming. ' kg',
                'a' => $forming
            ],
            [
                'y' => 'Corugation '. $corugation. ' kg',
                'a' => $corugation
            ]
        ];

        $data['total'] = $total; 
        $data['not'] = $not;
        $data['tahun'] = $tahun;
        $data['title'] = 'Beranda';
        return view('admin/home/index', $data);
    }
}
