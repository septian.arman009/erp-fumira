<?php

namespace App\Http\Controllers\Admin;

use App\Colored;
use App\Http\Controllers\Controller;
use App\IncDetail;
use App\Marketing;
use App\Ppic;
use App\SpkDetail;
use App\SpkColoredDetail;
use Auth;
use Datatables;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;

class PpicsController extends Controller
{

    public function index()
    {
        $data['title'] = 'Daftar SPK Pewarnaan Raw Material';
        return view('admin.ppic.coloring.index', $data);
    }

    public function table(Request $request, $type)
    {
        $ppics = Ppic::select(['id', 'marketing_id', 'wo_number', 'wo_date', 'user_id', 'status'])
                ->where('wo_type', $type)
                ->get();

        if($type == 'Base'){
            $destroy_url = 'colorings.destroy';
            $edit_url = 'colorings.edit';
        }else if($type == 'Laminating'){
            $destroy_url = 'cuttings.destroy';
            $edit_url = 'cuttings.edit';
        }else{
            $destroy_url = 'fgs.destroy';
            $edit_url = 'fgs.edit';
        }
    
        return Datatables::of($ppics)
            ->addColumn('do_number', function ($ppic) {
                return ucfirst($ppic->marketing->do_number);
            })
            ->addColumn('user', function ($ppic) {
                return ucfirst($ppic->user->name);
            })
            ->addColumn('action', function ($ppic) use ($destroy_url, $edit_url) {
                return view('admin.datatables._action', [
                    'id' => $ppic->id,
                    'destroy_url' => route($destroy_url, $ppic->id),
                    'edit_url' => route($edit_url, $ppic->id),
                    'confirm_message' => 'Apa anda yakin ingin menghapus tipe warna ' . $ppic->wo_number . ' ?',
                ]);
            })
            ->rawColumns(['do_number', 'user', 'action'])
            ->filter(function ($instance) use ($request) {
                if (!empty($request->get('do_number'))) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['do_number'], $request->get('do_number')) ? true : false;
                    });
                }
                if (!empty($request->get('wo_number'))) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['wo_number'], $request->get('wo_number')) ? true : false;
                    });
                }
                if (!empty($request->get('wo_date'))) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['wo_date'], $request->get('wo_date')) ? true : false;
                    });
                }
                if (!empty($request->get('user'))) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['user'], $request->get('user')) ? true : false;
                    });
                }
                if (!empty($request->get('status'))) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['status'], $request->get('status')) ? true : false;
                    });
                }
            })
            ->make(true);
    }

    public function create()
    {
        $data['stocks'] = IncDetail::all();
        $data['wo_number'] = $this->getId('ppics');
        $data['title'] = 'Tambah SPK Pewarnaan Raw Material';
        return view('admin.ppic.coloring.create', $data);
    }

    public function getId($table)
    {
        $last = DB::table($table)->latest()->first();

        if ($last) {
            return generateId($last->id + 1, 'SPK/RAW/');
        } else {
            return $id = 'SPK/RAW/00001';
        }
    }

    public function getLamId($table)
    {
        $last = DB::table($table)->latest()->first();

        if ($last) {
            return generateId($last->id + 1, 'SPK/LAM/');
        } else {
            return $id = 'SPK/LAM/00001';
        }
    }

    public function getFgsId($table)
    {
        $last = DB::table($table)->latest()->first();

        if ($last) {
            return generateId($last->id + 1, 'SPK/FGS/');
        } else {
            return $id = 'SPK/FGS/00001';
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'marketing_id' => 'required',
            'wo_number' => 'required|unique:ppics',
            'wo_date' => 'required',
        ]);

        if ($validator->passes()) {

            $data = $request->all();
            $data['wo_type'] = 'Base';
            $data['user_id'] = Auth::user()->id;
            $data['status'] = 'Waiting';
            $spk = Ppic::create($data);

            if (!is_null($request->raw)) {
                foreach ($request->raw as $key => $value) {
                    $spkDetail = [
                        'ppic_id' => $spk->id,
                        'inc_detail_id' => $value,
                    ];
                    SpkDetail::create($spkDetail);
                }
            }

            if(!is_null($request->colored) || !is_null($request->noncolored)){
                return response()->json(['success' => 'Berhasil menambahkan SPK Pewarnaan Raw Material : '.$request->wo_number]);
            }else{
                return response()->json(['error' => ['empty_raw' => ['Pilih minimal satu raw material']]]);
            }
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $title = 'Edit SPK Pewarnaan Raw Material';
        $spk = Ppic::find($id);
        $stocks = IncDetail::all();

        return view('admin.ppic.coloring.edit')->with(compact('spk', 'title', 'stocks'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'marketing_id' => 'required',
            'wo_number' => 'required|unique:ppics,wo_number,' . $id,
            'wo_date' => 'required',
        ]);

        if ($validator->passes()) {
            SpkDetail::where('ppic_id', $id)->delete();

            if (!is_null($request->raw)) {
                foreach ($request->raw as $key => $value) {
                    $spkDetail = [
                        'ppic_id' => $id,
                        'inc_detail_id' => $value,
                    ];
                    SpkDetail::create($spkDetail);
                }
            }

            return response()->json(['success' => 'Berhasil mengubah SPK Pewarnaan Raw Material']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function destroy($id)
    {
        Ppic::find($id)->delete();
        SpkDetail::where('ppic_id', $id)->delete();
        return response()->json(['id' => $id]);
    }

    public function doDetail($id)
    {
        $order = Marketing::where('id', $id)->first();
        $data = [
            'product_type' => $order->product->product_type,
            'type' => $order->product->type,
            'cap' => $order->product->cap,
            'color_name' => ($order->product->color_id != null) ? $order->product->color->color_name : 'Tanpa Warna',
            'thick' => $order->product->thick,
            'width' => $order->product->width,
            'length' => $order->product->length. ' mm',
            'qty' => $order->qty,
            'date_request' => $order->date_request,
            'date_send' => $order->date_send,
        ];

        return response()->json(['data' => $data]);
    }

    public function cuttingIndex(Request $request)
    {
        $data['title'] = 'Daftar SPK Potong Laminating';
        return view('admin.ppic.cutting.index', $data);
    }

    public function cuttingCreate()
    {
        $data['title'] = 'Tambah SPK Potong Laminating';
        $data['noncolored'] = IncDetail::all();
        $data['colored'] = Colored::all();
        $data['wo_number'] = $this->getLamId('ppics');
        return view('admin.ppic.cutting.create', $data);
    }

    public function cuttingStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'marketing_id' => 'required',
            'wo_number' => 'required|unique:ppics',
            'wo_date' => 'required',
        ]);

        if ($validator->passes()) {
            $data = $request->all();
            $data['wo_type'] = 'Laminating';
            $data['user_id'] = Auth::user()->id;
            $data['status'] = 'Waiting';

            $marketing = Marketing::find($request->marketing_id);

            if (!is_null($request->noncolored)) {
                if(is_null($marketing->product->color)){
                    $spk = Ppic::create($data);
                    foreach ($request->noncolored as $key => $value) {
                        $non = [
                            'ppic_id' => $spk->id,
                            'inc_detail_id' => $value,
                        ];
                        SpkDetail::create($non);
                    }
                    return response()->json(['success' => 'Berhasil menambah SPK Potong Laminating : '.$request->wo_number]);
                }else{
                    return response()->json(['error' => ['spk_error' => ['SPK tersebut adalah SPK Produk Warna']]]);
                    die();
                }
            }

            if (!is_null($request->colored)) {
                if(!is_null($marketing->product->color)){
                    $spk = Ppic::create($data);
                    foreach ($request->colored as $key => $value) {
                        $colored = [
                            'ppic_id' => $spk->id,
                            'colored_id' => $value,
                        ];
                        SpkColoredDetail::create($colored);
                    }
                    return response()->json(['success' => 'Berhasil menambah SPK Potong Laminating : '.$request->wo_number]);
                }else{
                    return response()->json(['error' => ['spk_error' => ['SPK tersebut adalah SPK Produk Tanpa Warna']]]);
                    die();
                }
            }
            
            if(is_null($request->colored) && is_null($request->noncolored)){
                return response()->json(['error' => ['empty_raw' => ['Pilih minimal satu raw material']]]);
            }
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function cuttingEdit($id)
    {
        $title = 'Edit SPK Potong Laminating';
        $spk = Ppic::where('id', $id)->first();
        $noncolored = IncDetail::all();
        $colored = Colored::all();
        return view('admin.ppic.cutting.edit')->with(compact('spk', 'title', 'noncolored', 'colored'));
    }

    public function cuttingUpdate(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'marketing_id' => 'required',
            'wo_number' => 'required|unique:ppics,wo_number,' . $id,
            'wo_date' => 'required',
        ]);

        if ($validator->passes()) {
            SpkDetail::where('ppic_id', $id)->delete();

            if (!is_null($request->noncolored)) {
                foreach ($request->noncolored as $key => $value) {
                    $spkNon = [
                        'ppic_id' => $id,
                        'inc_detail_id' => $value,
                    ];
                    SpkDetail::create($spkNon);
                }
            }

            SpkColoredDetail::where('ppic_id', $id)->delete();

            if (!is_null($request->color)) {
                foreach ($request->color as $key => $value) {
                    $spkColor = [
                        'ppic_id' => $id,
                        'colored_id' => $value,
                    ];
                    SpkColoredDetail::create($spkColor);
                }
            }

            return response()->json(['success' => 'Berhasil mengubah SPK Potong Laminating']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function cuttingDestroy($id)
    {
        Ppic::find($id)->delete();
        SpkDetail::where('ppic_id', $id)->delete();
        SpkColoredDetail::where('ppic_id', $id)->delete();
        return response()->json(['id' => $id]);
    }

    public function fgsINdex(Request $request)
    {
        $data['title'] = 'Daftar SPK FGS';
        return view('admin.ppic.fgs.index', $data);
    }

    public function fgsCreate()
    {
        $data['title'] = 'Tambah SPK FGS';
        $data['wo_number'] = $this->getFgsId('ppics');
        return view('admin.ppic.fgs.create', $data);
    }

    public function fgsStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'marketing_id' => 'required',
            'wo_number' => 'required|unique:ppics',
            'wo_date' => 'required',
            'qty_spk' => 'required'
        ]);

        if ($validator->passes()) {
            $data = $request->all();
            $data['wo_type'] = 'FGS';
            $data['user_id'] = Auth::user()->id;
            $data['status'] = 'Waiting';
            $data['qty_spk'] = $request->qty_spk;

            $marketing = Marketing::find($request->marketing_id);

            if($request->qty_spk <=$marketing->qty){
                $spk = Ppic::create($data);
                return response()->json(['success' => 'Berhasil menambah SPK FGS']);
            }else{
                return response()->json(['error' => ['spk_error' => ['Jumlah harus sama atau kurang dari '.$marketing->qty]]]);
            }
           
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function fgsEdit($id)
    {
        $title = 'Edit SPK FGS';
        $spk = Ppic::where('id', $id)->first();
        return view('admin.ppic.fgs.edit')->with(compact('spk', 'title'));
    }

    public function fgsUpdate(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'marketing_id' => 'required',
            'wo_number' => 'required|unique:ppics,wo_number,' . $id,
            'wo_date' => 'required',
            'qty_spk' => 'required'
        ]);

        if ($validator->passes()) {
            $spk = Ppic::find($id);
            $data = $request->all();
            $data['user_id'] = Auth::user()->id;
            $data['qty_spk'] = $request->qty_spk;

            $marketing = Marketing::find($request->marketing_id);

            if($request->qty_spk <=$marketing->qty){
                $spk->update($data);
                return response()->json(['success' => 'Berhasil mengubah SPK FGS']);
            }else{
                return response()->json(['error' => ['spk_error' => ['Jumlah harus sama atau kurang dari '.$marketing->qty]]]);
            }
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function fgsDestroy($id)
    {
        Ppic::find($id)->delete();
        return response()->json(['id' => $id]);
    }

    
}
