<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Datatables;
use Illuminate\Support\Str;
use Validator;
use App\Product;
use Auth;

class ProductsController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $products = Product::select(['id', 'product_type', 'type', 'cap', 'thick', 'width', 'length', 'gramatur', 'color_id', 'user_id'])->get();
            return Datatables::of($products)
                ->addColumn('user', function ($product) {
                    return ucfirst($product->user->name);
                })
                ->addColumn('color_name', function ($product) {
                    if($product->color){
                        return $product->color->color_name;
                    }else{
                        return 'Tanpa Warna';
                    }
                })
                ->addColumn('action', function ($product) {
                    return view('admin.datatables._action', [
                        'id' => $product->id,
                        'destroy_url' => route('products.destroy', $product->id),
                        'edit_url' => route('products.edit', $product->id),
                        'confirm_message' => 'Apa anda yakin ingin menghapus tipe warna ' . $product->product_type . ' ?',
                    ]);
                })
                ->rawColumns(['color_name', 'user', 'action'])
                ->filter(function ($instance) use ($request) { 
                    if (!empty($request->get('type'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['type'], $request->get('type')) ? true : false;
                        });
                    }
                    if (!empty($request->get('qty'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['qty'], $request->get('qty')) ? true : false;
                        });
                    }
                    if (!empty($request->get('cap'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['cap'], $request->get('cap')) ? true : false;
                        });
                    }
                    if (!empty($request->get('thick'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['thick'], $request->get('thick')) ? true : false;
                        });
                    }
                    if (!empty($request->get('width'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['width'], $request->get('width')) ? true : false;
                        });
                    }
                    if (!empty($request->get('length'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['length'], $request->get('length')) ? true : false;
                        });
                    }
                    if (!empty($request->get('color_name'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['color_name'], $request->get('color_name')) ? true : false;
                        });
                    }
                    if (!empty($request->get('user'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['user'], $request->get('user')) ? true : false;
                        });
                    }
                })
                ->make(true);
        }

        $data['title'] = 'Daftar Tipe Produk';
        return view('admin.product.index', $data);
    }

    public function create()
    {
        $data['title'] = 'Tambah Tipe Produk';
        return view('admin.product.create', $data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_type' => 'required|unique:products',
            'type' => 'required',
            'cap' => 'required',
            'thick' => 'required',
            'width' => 'required',
            'length' => 'required',
            'gramatur' => 'required'
            ]);

        if ($validator->passes()) {
            $data = $request->all();
            $data['user_id'] = Auth::user()->id;
            $product = Product::create($data);
            return response()->json(['success' => 'Berhasil menambahkan Tipe Produk']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $title = 'Edit Tipe Produk';
        $product = Product::find($id);
        return view('admin.product.edit')->with(compact('product', 'title'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'product_type' => 'required|unique:products,product_type,'.$id,
            'type' => 'required',
            'cap' => 'required',
            'thick' => 'required',
            'width' => 'required',
            'length' => 'required',
            'gramatur' => 'required'
        ]);

        if ($validator->passes()) {
            $product = Product::find($id);
            $product->update($request->all());
            return response()->json(['success' => 'Berhasil mengubah data Tipe Produk']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function destroy($id)
    {
        $product = Product::where('id', $id)->delete();
        return response()->json(['id' => $id]);
    }
}
