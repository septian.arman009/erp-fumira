<?php

namespace App\Http\Controllers\Admin;

use App\Color;
use App\Colored;
use App\Http\Controllers\Controller;
use App\IncDetail;
use App\Ppic;
use App\SpkDetail;
use Illuminate\Http\Request;
use Validator;
use App\Laminating;
use App\Marketing;
use App\Fgs;
use Auth;

class ProductionsController extends Controller
{
    public function baseColored()
    {
        $data['title'] = 'Proses Pewarnaan Base Material';
        $data['onproccess'] = IncDetail::where('status', 'On Proccess')->first();
        return view('admin.production.base.base_colored', $data);
    }

    public function tableStatus()
    {
        $data['spk'] = Ppic::where('wo_type', 'Base')->get();
        return view('admin.production.base._table-status', $data);
    }

    public function doSpk($id)
    {
        $html = '';
        $html .= '<select name="ppic_id" id="ppic_id" onchange="doraw()" class="form-control">';

        if ($id != 'null') {
            $spks = Ppic::where([
                ['marketing_id', $id],
                ['wo_type', 'Base'],
            ])->get();
            foreach ($spks as $spk) {
                $html .= '<option value="' . $spk->id . '">' . $spk->wo_number . '</option>';
            }
        }

        $html .= '</select>';

        return $html;
    }

    public function doRaw($id)
    {
        $html = '';
        $html .= '<select name="inc_detail_id" class="form-control">';

        if ($id != 'null') {
            $ppic = Ppic::where('id', $id)->first();
            foreach ($ppic->spkdetail as $spk) {
                $inc_id[] = $spk->inc_detail_id;
            }
        }

        $inc_detail = IncDetail::whereIn('id', $inc_id)->get();

        foreach ($inc_detail as $inc) {
            if ($inc->length > 0) {
                $html .= '<option value="' . $inc->id . '">' . $inc->raw_pcn . '</option>';
            }
        }

        $html .= '</select>';

        return $html;
    }

    public function baseProccess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'marketing_id' => 'required',
            'ppic_id' => 'required',
            'inc_detail_id' => 'required',
        ]);

        if ($validator->passes()) {

            $ppic = Ppic::where('id', $request->ppic_id)->first();
            $ppic->status = 'On Proccess';
            $ppic->save();

            $inc_detail = IncDetail::where('id', $request->inc_detail_id)->first();
            $inc_detail->status = 'On Proccess';
            $inc_detail->save();

            return response()->json(['success' => 'Berhasil mengubah status ' . $inc_detail->raw_pcn . ' pada ' . $ppic->wo_number]);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function rawColoredStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'inc_detail_id' => 'required',
            'color_id' => 'required',
            'length' => 'required|lte:' . $request->inc_length,
        ]);

        if ($validator->passes()) {
            $base = IncDetail::where('id', $request->inc_detail_id)->first();
            $base->length = $base->length - $request->length;
            if ($base->length == 0) {
                $base->status = 'Closed';
            } else {
                $base->status = 'Rest Of';
            }
            $base->save();

            $colored_pcn = $base->raw_pcn. '-' . $request->machine_code;
            $data = [
                'inc_detail_id' => $request->inc_detail_id,
                'color_id' => $request->color_id,
                'colored_pcn' => $colored_pcn,
                'length' => $request->length,
                'status' => 'Ready',
            ];

            $colored = Colored::create($data);

            $ppic = $base->spkdetail->ppic;

            foreach ($ppic->spkdetail as $spk) {
                $inc_id[] = $spk->inc_detail_id;
            }

            $rest_of = IncDetail::whereIn('id', $inc_id)->get()->sum('length');

            if ($rest_of == 0) {
                $ppic->status = 'Closed';
            } else {
                $ppic->status = 'Waiting';
            }

            $ppic->save();

            return response()->json(['success' => 'Berhasil memproses pewarnaan Raw Material ' . $colored_pcn]);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function laminating()
    {
        $data['title'] = 'Proses Potong Laminating';
        $data['noncolored'] = IncDetail::where('status', 'On Proccess')->first();
        $data['colored'] = Colored::where('status', 'On Proccess')->first();
        return view('admin.production.laminating.laminating', $data);
    }

    public function doSpkLaminating($id)
    {
        $html = '';
        $html .= '<select name="ppic_id" id="ppic_id" class="form-control" onchange="doraw()">';

        if ($id != 'null') {
            $spks = Ppic::where([
                ['marketing_id', $id],
                ['wo_type', 'Laminating']
            ])->get();
           
            if(count($spks) > 0){
                foreach ($spks as $spk) {
                    $html .= '<option value="' . $spk->id . '">' . $spk->wo_number . '</option>';
                }
            }else{
                $html .= '<option value="null">Tidak ada SPK</option>';
            }
        }

        $html .= '</select>';

        return $html;
    }

    public function doRawLaminating($color, $ppic_id)
    {
        $html = '';
        $html .= '<select name="raw_id" id="raw_id" class="form-control">';
        if ($color == 'NonColored') {
            if ($ppic_id != 'null') {
                $ppic = Ppic::where('id', $ppic_id)->first();
                if ($ppic->spkdetail) {
                    $inc_id = array();
                    foreach ($ppic->spkdetail as $spk) {
                        $inc_id[] = $spk->inc_detail_id;
                    }

                    if(count($inc_id) > 0){
                        $inc_detail = IncDetail::whereIn('id', $inc_id)->get();
                        foreach ($inc_detail as $inc) {
                            if ($inc->length > 0) {
                                $html .= '<option value="' . $inc->id . '">' . $inc->raw_pcn . '</option>';
                            }
                        }
                    }
                    
                }
            }
        } else {
            if ($ppic_id != 'null') {
                $ppic = Ppic::where('id', $ppic_id)->first();
                if ($ppic->spkcoloreddetail) {
                    $colored_id = array();
                    foreach ($ppic->spkcoloreddetail as $spk) {
                        $colored_id[] = $spk->colored_id;
                    }

                    if(count($colored_id) > 0){
                        $colored = Colored::whereIn('id', $colored_id)->get();
                        foreach ($colored as $col) {
                            if ($col->length > 0) {
                                $html .= '<option value="' . $col->id . '">' . $col->colored_pcn . '</option>';
                            }
                        }
                    }
                    
                }
            }
        }

        $html .= '</select>';

        return $html;
    }

    public function tableStatusLaminatingNon()
    {
        $data['spk'] = Ppic::where('wo_type', 'Laminating')->get();
        return view('admin.production.laminating._table-non', $data);
    }

    public function tableStatusLaminatingColored()
    {
        $data['spk'] = Ppic::where('wo_type', 'Laminating')->get();
        return view('admin.production.laminating._table-colored', $data);
    }

    public function laminatingProccess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'marketing_id' => 'required',
            'ppic_id' => 'required',
            'color_type' => 'required',
            'raw_id' => 'required',
        ]);

        if ($validator->passes()) {

            $ppic = Ppic::where('id', $request->ppic_id)->first();
            $ppic->status = 'On Proccess';
            $ppic->save();

            if($request->color_type == 'Colored'){
                $raw_detail = Colored::where('id', $request->raw_id)->first();
                $raw_detail->status = 'On Proccess';
                $raw_detail->save();
                $pcn = $raw_detail->colored_pcn;
            }else{
                $raw_detail = IncDetail::where('id', $request->raw_id)->first();
                $raw_detail->status = 'On Proccess';
                $raw_detail->save();
                $pcn = $raw_detail->raw_pcn;
            }
            

            return response()->json(['success' => 'Berhasil mengubah status ' . $pcn . ' pada ' . $ppic->wo_number]);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function laminatingColoredStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'colored_id' => 'required',
            'length' => 'required|lte:' . ($request->colored_length*1000).'|gt:0',
        ]);

        if ($validator->passes()) {
            $colored = Colored::where('id', $request->colored_id)->first();
            $colored->length = $colored->length - ($request->length/1000);
           
            if ($colored->length == 0) {
                $colored->status = 'Closed';
            }

            $colored->save();
            $pcn = $colored->colored_pcn;
            $marketing_id = $colored->spkcoloreddetail->ppic->marketing_id;
            $last_lot = $this->autolot();
            $data = [
                'marketing_id' => $marketing_id,
                'pcn' => $pcn.'-LAM.',
                'lot' => $last_lot,
                'length' => $request->length,
                'user_id' => Auth::user()->id,
                'color_type' => 'Colored',
                'description' => $request->description,
                'status' => $request->status,
            ];
            
            Laminating::create($data);

            $ppic = $colored->spkcoloreddetail->ppic;

            foreach ($ppic->spkcoloreddetail as $spk) {
                $col_id[] = $spk->id;
            }

            $rest_of = Colored::whereIn('id', $col_id)->get()->sum('length');

            if ($rest_of == 0) {
                $ppic->status = 'Closed';
            }

            $ppic->save();

            return response()->json(['success' => 'Berhasil memproses produk ' . $pcn. '-LAM.'.lot($last_lot)]);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function laminatingColoredStoreFinish(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'colored_id' => 'required'
        ]);

        if ($validator->passes()) {
            $colored = Colored::where('id', $request->colored_id)->first();
           
            if ($colored->length == 0) {
                $colored->status = 'Closed';
            }else{
                $colored->status = 'Ready';
            }

            $colored->save();

            $ppic = $colored->spkcoloreddetail->ppic;

            foreach ($ppic->spkcoloreddetail as $spk) {
                $col_id[] = $spk->id;
            }

            $rest_of = Colored::whereIn('id', $col_id)->get()->sum('length');

            if ($rest_of == 0) {
                $ppic->status = 'Closed';
            } else {
                $ppic->status = 'Waiting';
            }

            $ppic->save();

            return response()->json(['success' => 'Aksi berhasil']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function laminatingNonColoredStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'inc_detail_id' => 'required',
            'length' => 'required|lte:' .($request->inc_length*1000).'|gt:0'
        ]);

        if ($validator->passes()) {
            $base = IncDetail::where('id', $request->inc_detail_id)->first();
            $base->length = $base->length - ($request->length/1000);
            if ($base->length == 0) {
                $base->status = 'Closed';
            }
            $base->save();

            $marketing_id = $base->spkdetail->ppic->marketing_id;
            $pcn = $base->raw_pcn;
            $last_lot = $this->autolot();
            $data = [
                'marketing_id' => $marketing_id,
                'pcn' => $pcn.'-LAM.',
                'lot' => $last_lot,
                'length' => $request->length,
                'user_id' => Auth::user()->id,
                'color_type' => 'Non Color',
                'description' => $request->description,
                'status' => $request->status,
            ];
            
            Laminating::create($data);

            $ppic = $base->spkdetail->ppic;

            foreach ($ppic->spkdetail as $spk) {
                $inc_id[] = $spk->inc_detail_id;
            }

            $rest_of = IncDetail::whereIn('id', $inc_id)->get()->sum('length');

            if ($rest_of == 0) {
                $ppic->status = 'Closed';
            }

            $ppic->save();

            return response()->json(['success' => 'Berhasil memproses produk  ' . $pcn. '-LAM.' .lot($last_lot)]);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function laminatingNonColoredStoreFinish(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'inc_detail_id' => 'required'
        ]);

        if ($validator->passes()) {
            $base = IncDetail::where('id', $request->inc_detail_id)->first();
            $base->length = $base->length - ($request->length/1000);
            if ($base->length == 0) {
                $base->status = 'Closed';
            } else {
                $base->status = 'Ready';
            }
            $base->save();

            $ppic = $base->spkdetail->ppic;

            foreach ($ppic->spkdetail as $spk) {
                $inc_id[] = $spk->inc_detail_id;
            }

            $rest_of = IncDetail::whereIn('id', $inc_id)->get()->sum('length');

            if ($rest_of == 0) {
                $ppic->status = 'Closed';
            } else {
                $ppic->status = 'Waiting';
            }

            $ppic->save();

            return response()->json(['success' => 'Aksi berhasil']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    
    public function fgs()
    {
        $data['title'] = 'Proses Potong FGS';
        $data['proccess'] = Ppic::where([
            ['wo_type', 'FGS'],
            ['status', 'On Proccess']
        ])->first();
        
        if($data['proccess']){
            $color = ($data['proccess']->marketing->product->color_id != NULL) ? 'Colored' : 'Non Color';
            $data['laminatings'] = Laminating::where([
                ['color_type', $color],
                ['marketing_id', $data['proccess']->marketing_id],
                ['status', 'OK']
            ])->get();
        }
        return view('admin.production.fgs.fgs', $data);
    }

    public function doSpkFgs($id)
    {
        $html = '';
        $html .= '<select name="ppic_id" id="ppic_id" class="form-control">';

        if ($id != 'null') {
            $spks = Ppic::where([
                ['marketing_id', $id],
                ['wo_type', 'FGS'],
                ['qty_spk' , '>', 0]
            ])->get();
           
            if(count($spks) > 0){
                foreach ($spks as $spk) {
                    $html .= '<option value="' . $spk->id . '">' . $spk->wo_number . '</option>';
                }
            }else{
                $html .= '<option value="null">Tidak ada SPK</option>';
            }
        }

        $html .= '</select>';

        return $html;
    }

    public function fgsProccess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'marketing_id' => 'required',
            'ppic_id' => 'required'
        ]);

        if ($validator->passes()) {

            $ppic = Ppic::where('id', $request->ppic_id)->first();
            $ppic->status = 'On Proccess';
            $ppic->save();

            return response()->json(['success' => 'Berhasil mengubah status pada ' . $ppic->wo_number]);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }
    
    public function fgsStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'marketing_id' => 'required',
            'laminating_id' => 'required',
            'ppic_id' => 'required',
            'pcn' => 'required',
            'status' => 'required'
        ]);

        if ($validator->passes()) {
            $laminating = Laminating::where('id', $request->laminating_id)->first();
            $laminating->status = 'Finish';
            $laminating->save();

            $marketing = Marketing::where('id', $request->marketing_id)->first();
            $marketing->qty = $marketing->qty - 1;
            $marketing->save();

            $ppic = Ppic::where('id', $request->ppic_id)->first();
            $ppic->qty_spk = $ppic->qty_spk - 1;
            $ppic->save();

            $data = [
                'marketing_id' => $marketing->id,
                'laminating_id' => $request->laminating_id,
                'ppic_id' => $request->ppic_id,
                'pcn' => $request->pcn,
                'lot' => $this->autolotFgs($request->pcn),
                'class' => $request->class,
                'length' => $laminating->length,
                'user_id' => Auth::user()->id,
                'color_type' => $laminating->color_type,
                'description' => $request->description,
                'status' => $request->status
            ];

            $fgs = Fgs::create($data);

            return response()->json(['success' => 'Berhasil memproses barang FGS']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function fgsStoreFinish(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ppic_id' => 'required'
        ]);

        if ($validator->passes()) {
            $ppic = Ppic::where('id', $request->ppic_id)->first();
            if($ppic->qty_spk > 0){
                $ppic->status = 'Waiting';
            }else{
                $ppic->status = 'Closed';
            }
            $ppic->save();

            return response()->json(['success' => 'Aksi berhasil']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }
    
    public function autolot()
    {
        $last = Laminating::whereMonth('created_at', date('m'))
        ->whereYear('created_at', date('Y'))
        ->latest()
        ->first();

        if($last){
            $last_id = $last->lot + 1;
            return $last_id;
        }else{
            $last_id = 1;
            return $last_id;
        }
    }

    public function autolotFgs($pcn)
    {
        $last = Fgs::whereMonth('created_at', date('m'))
        ->where('pcn', $pcn)
        ->whereYear('created_at', date('Y'))
        ->latest()
        ->first();

        if($last){
            $last_id = $last->lot + 1;
            return $last_id;
        }else{
            $last_id = 1;
            return $last_id;
        }
    }

}
