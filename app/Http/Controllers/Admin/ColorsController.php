<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Color;
use Auth;
use Datatables;
use Illuminate\Support\Str;
use Validator;

class ColorsController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $colors = Color::select(['id', 'color_name', 'color_code', 'user_id'])->get();
            return Datatables::of($colors)
                ->addColumn('user', function ($color) {
                    return ucfirst($color->user->name);
                })
                ->addColumn('action', function ($color) {
                    return view('admin.datatables._action', [
                        'id' => $color->id,
                        'destroy_url' => route('colors.destroy', $color->id),
                        'edit_url' => route('colors.edit', $color->id),
                        'confirm_message' => 'Apa anda yakin ingin menghapus tipe warna ' . $color->color_name . ' ?',
                    ]);
                })
                ->rawColumns(['user', 'action'])
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('color_name'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['color_name'], $request->get('color_name')) ? true : false;
                        });
                    }
                    if (!empty($request->get('color_code'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['color_code'], $request->get('color_code')) ? true : false;
                        });
                    }
                    if (!empty($request->get('user'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['user'], $request->get('user')) ? true : false;
                        });
                    }
                })
                ->make(true);
        }

        $data['title'] = 'Daftar Tipe Warna';
        return view('admin.color.index', $data);
    }

    public function create()
    {
        $data['title'] = 'Tambah Tipe Warna';
        return view('admin.color.create', $data);
    }

    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'color_name' => 'required|unique:colors',
        ]);

        if ($validator->passes()) {
            $data = $request->all();
            $data['user_id'] = Auth::user()->id;
            $color = Color::create($data);
            return response()->json(['success' => 'Berhasil menambahkan Tipe Warna']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $title = 'Edit Tipe Warna';
        $color = Color::find($id);
        return view('admin.color.edit')->with(compact('color', 'title'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'color_name' => 'required|unique:colors,color_name,' . $id,
        ]);

        if ($validator->passes()) {
            $color = Color::find($id);
            $color->update($request->all());
            return response()->json(['success' => 'Berhasil mengubah data Tipe Warna']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }
    
    public function destroy($id)
    {
        $color = Color::where('id', $id)->delete();
        return response()->json(['id' => $id]);
    }
}
