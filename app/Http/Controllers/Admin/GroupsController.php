<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Group;
use Datatables;
use Illuminate\Suppoer\Str;
use Auth;

class GroupsController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $groups = Group::select(['id', 'machine_name', 'machine_code', 'user_id'])->get();
            return Datatables::of($groups)
                ->addColumn('user', function ($group) {
                    return ucfirst($group->user->name);
                })
                ->addColumn('action', function ($group) {
                    return view('admin.datatables._action', [
                        'id' => $group->id,
                        'destroy_url' => route('groups.destroy', $group->id),
                        'edit_url' => route('groups.edit', $group->id),
                        'confirm_message' => 'Apa anda yakin ingin menghapus mesin ' . $group->machinde_name . ' ?',
                    ]);
                })
                ->rawColumns(['user', 'action'])
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('machinde_name'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['machinde_name'], $request->get('machinde_name')) ? true : false;
                        });
                    }
                    if (!empty($request->get('machinde_code'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['machinde_code'], $request->get('machinde_code')) ? true : false;
                        });
                    }
                    if (!empty($request->get('user'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['user'], $request->get('user')) ? true : false;
                        });
                    }
                })
                ->make(true);
        }

        $data['title'] = 'Daftar Mesin';
        return view('admin.group.index', $data);
    }

    public function create()
    {
        $data['title'] = 'Tambah Mesin';
        return view('admin.group.create', $data);
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'machine_name' => 'required|unique:groups',
            'machine_code' => 'required|unique:groups'
        ]);

        if ($validator->passes()) {
            $data = $request->all();
            $data['user_id'] = Auth::user()->id;
            $group = Group::create($data);
            return response()->json(['success' => 'Berhasil menambahkan Mesin']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $title = 'Edit Mesin';
        $group = Group::find($id);
        return view('admin.group.edit')->with(compact('group', 'title'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'machine_name' => 'required|unique:groups,machine_name,' . $id,
            'machine_code' => 'required|unique:groups,machine_code,' . $id
        ]);

        if ($validator->passes()) {
            $group = Group::find($id);
            $group->update($request->all());
            return response()->json(['success' => 'Berhasil mengubah data Mesin']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function destroy($id)
    {
        $group = Group::where('id', $id)->delete();
        return response()->json(['id' => $id]);
    }
}
