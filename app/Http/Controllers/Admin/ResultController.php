<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Fgs;
use App\Laminating;
use Datatables;
use Illuminate\Support\Str;

class ResultController extends Controller
{
    public function index($tahun, $table)
    {   

        $data['table'] = $table;
        $data['tahun'] = $tahun;
        $data['title'] = 'Hasil Produksi';
        return view('admin.result.index', $data);
    }

    public function fgs(Request $request, $tahun)
    {
        if ($request->ajax()) {
            $fgs = Fgs::select(['id', 'marketing_id', 'pcn', 'lot', 'class', 'length', 'user_id', 'color_type', 'status', 'created_at'])
            ->whereYear('created_at', '=', $tahun)
            ->get();
        
            return Datatables::of($fgs)
                ->addColumn('do_number', function ($fg) {
                    return $fg->marketing->do_number;
                })
                ->addColumn('product_type', function ($fg) {
                    return $fg->marketing->product->product_type;
                })
                ->addColumn('pcn', function ($fg) {
                    return $fg->pcn.'-'.lot($fg->lot);
                })
                ->addColumn('length', function ($fg) {
                    return $fg->length;
                })
                ->addColumn('class', function ($fg) {
                    return 'Kelas '.$fg->class;
                })
                ->addColumn('color_type', function ($fg) {
                    if($fg->color_type == 'Colored'){
                        return '<p style="color:green;font-weight:bold">'.$fg->color_type.'</p>';
                    }else{
                        return '<p style="color:orange;font-weight:bold">'.$fg->color_type.'</p>';
                    }
                })
                ->addColumn('date', function ($fg) {
                    return indoFullDateDay($fg->created_at);
                })
                ->rawColumns(['do_number', 'product_type', 'pcn', 'length', 'class', 'color_type', 'date'])
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('do_number'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['do_number'], $request->get('do_number')) ? true : false;
                        });
                    }
                    if (!empty($request->get('product_type'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['product_type'], $request->get('product_type')) ? true : false;
                        });
                    }
                    if (!empty($request->get('pcn'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['pcn'], $request->get('pcn')) ? true : false;
                        });
                    }
                    if (!empty($request->get('length'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return ($row['length'] == $request->get('length')) ? true : false;
                        });
                    }
                    if (!empty($request->get('class'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['class'], $request->get('class')) ? true : false;
                        });
                    }
                    if (!empty($request->get('color_type'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['color_type'], $request->get('color_type')) ? true : false;
                        });
                    }
                    if (!empty($request->get('date'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['date'], $request->get('date')) ? true : false;
                        });
                    }
                    if (!empty($request->get('status'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['status'], $request->get('status')) ? true : false;
                        });
                    }
                })
                ->make(true);
        }
    }

    public function laminating(Request $request, $tahun)
    {
        if ($request->ajax()) {
            $laminatings = Laminating::select(['id', 'marketing_id', 'pcn', 'lot', 'length', 'user_id', 'color_type', 'status', 'created_at'])
            ->whereYear('created_at', '=', $tahun)
            ->get();
        
            return Datatables::of($laminatings)
                ->addColumn('do_number', function ($lm) {
                    return $lm->marketing->do_number;
                })
                ->addColumn('product_type', function ($lm) {
                    return $lm->marketing->product->product_type;
                })
                ->addColumn('pcn', function ($lm) {
                    return $lm->pcn.'-'.lot($lm->lot);
                })
                ->addColumn('length', function ($lm) {
                    return $lm->length;
                })
                ->addColumn('color_type', function ($lm) {
                    if($lm->color_type == 'Colored'){
                        return '<p style="color:green;font-weight:bold">'.$lm->color_type.'</p>';
                    }else{
                        return '<p style="color:orange;font-weight:bold">'.$lm->color_type.'</p>';
                    }
                })
                ->addColumn('date', function ($lm) {
                    return indoFullDateDay($lm->created_at);
                })
                ->rawColumns(['do_number', 'product_type', 'pcn', 'length', 'color_type', 'date'])
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('do_number'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['do_number'], $request->get('do_number')) ? true : false;
                        });
                    }
                    if (!empty($request->get('product_type'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['product_type'], $request->get('product_type')) ? true : false;
                        });
                    }
                    if (!empty($request->get('pcn'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['pcn'], $request->get('pcn')) ? true : false;
                        });
                    }
                    if (!empty($request->get('length'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return ($row['length'] == $request->get('length')) ? true : false;
                        });
                    }
                    if (!empty($request->get('color_type'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['color_type'], $request->get('color_type')) ? true : false;
                        });
                    }
                    if (!empty($request->get('date'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['date'], $request->get('date')) ? true : false;
                        });
                    }
                })
                ->make(true);
        }

    }
}
