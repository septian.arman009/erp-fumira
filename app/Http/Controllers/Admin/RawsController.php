<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Datatables;
use Validator;
use App\Raw;
use Auth;

class RawsController extends Controller
{
   
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $raws = Raw::select(['id', 'raw_type', 'spec', 'thick', 'width', 'user_id'])->get();
            return Datatables::of($raws)
                ->addColumn('user', function ($raw) {
                    return ucfirst($raw->user->name);
                })
                ->addColumn('action', function ($raw) {
                    return view('admin.datatables._action', [
                        'id' => $raw->id,
                        'destroy_url' => route('raws.destroy', $raw->id),
                        'edit_url' => route('raws.edit', $raw->id),
                        'confirm_message' => 'Yakin ingin menghapus tipe raw material ' . $raw->raw_type . ' ?',
                    ]);
                })
                ->rawColumns(['user', 'action'])
                ->filter(function ($instance) use ($request) {
                    if (!empty($request->get('raw_type'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['raw_type'], $request->get('raw_type')) ? true : false;
                        });
                    }
                    if (!empty($request->get('spec'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['spec'], $request->get('spec')) ? true : false;
                        });
                    }
                    if (!empty($request->get('thick'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['thick'], $request->get('thick')) ? true : false;
                        });
                    }
                    if (!empty($request->get('width'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['width'], $request->get('width')) ? true : false;
                        });
                    }
                    if (!empty($request->get('user'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            return Str::contains($row['user'], $request->get('user')) ? true : false;
                        });
                    }

                })
                ->make(true);
        }

        $data['title'] = 'Daftar Tipe Raw Material';
        return view('admin.raw.index', $data);
    }

    public function create()
    {
        $data['title'] = 'Tambah Tipe Raw Material';
        return view('admin.raw.create', $data);
    }

 
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'raw_type' => 'required|unique:raws',
            'spec' => 'required',
            'thick' => 'required',
            'width' => 'required'
        ]);

        if ($validator->passes()) {
            $data = $request->all();
            $data['user_id'] = Auth::user()->id;

            $raw = Raw::create($data);
            return response()->json(['success' => 'Tipe Raw Material berhasil ditambahkan']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {   
        $title = 'Edit Tipe Raw Material';
        $raw = Raw::find($id);
        return view('admin.raw.edit')
            ->with(compact('raw', 'title'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'raw_type' => 'required|unique:raws,raw_type,' . $id,
            'spec' => 'required',
            'thick' => 'required',
            'width' => 'required'
        ]);

        if ($validator->passes()) {
            $raw = Raw::find($id);
            $raw->update($request->all());
            return response()->json(['success' => 'Berhasil mengubah data Tipe Raw Material']);
        } else {
            return response()->json(['error' => $validator->getMessageBag()->toArray()]);
        }
    }

    public function destroy($id)
    {
        $raw = Raw::where('id', $id)->delete();
        return response()->json(['id' => $id]);

    }
}
