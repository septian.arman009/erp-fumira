<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ppic extends Model
{
    
    protected $fillable = ['marketing_id', 'wo_number', 'wo_type', 'wo_date', 'user_id', 'status', 'qty_spk'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function marketing()
    {
        return $this->belongsTo('App\Marketing');
    }

    public function spkdetail()
    {
        return $this->hasMany('App\SpkDetail');
    }

    public function spkcoloreddetail()
    {
        return $this->hasMany('App\SpkColoredDetail');
    }
    
}
