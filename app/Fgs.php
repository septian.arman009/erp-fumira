<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fgs extends Model
{
    protected $fillable = [
        'marketing_id',
        'laminating_id',
        'ppic_id',
        'pcn',
        'lot',
        'class',
        'length',
        'user_id',
        'color_type',
        'description',
        'status'
    ];

    public function marketing()
    {
        return $this->belongsTo('App\Marketing');
    }
}
