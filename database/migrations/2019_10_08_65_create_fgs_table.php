<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fgs', function (Blueprint $table) {
            $table->increments('id', 10);
            $table->integer('marketing_id')->length(10)->unsigned();
            $table->integer('laminating_id')->length(10)->unsigned();
            $table->integer('ppic_id')->length(10)->unsigned();
            $table->string('pcn', 10);
            $table->integer('lot')->length(10);
            $table->integer('class')->length(2);
            $table->integer('length')->length(10);
            $table->integer('user_id')->length(10)->unsigned();
            $table->string('color_type', 15);
            $table->string('description', 250)->nullable();
            $table->string('status', 25);
            $table->timestamps();

            $table->foreign('marketing_id')->references('id')->on('marketings')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('laminating_id')->references('id')->on('laminatings')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('ppic_id')->references('id')->on('ppics')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fgs_coloreds');
    }
}
