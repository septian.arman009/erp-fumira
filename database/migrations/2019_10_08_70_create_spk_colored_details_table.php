<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpkColoredDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spk_colored_details', function (Blueprint $table) {
            $table->increments('id', 10);
            $table->integer('ppic_id')->length(10)->unsigned();
            $table->integer('colored_id')->length(10)->unsigned();
            $table->timestamps();

            $table->foreign('ppic_id')->references('id')->on('ppics')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('colored_id')->references('id')->on('coloreds')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spk_colored_details');
    }
}
