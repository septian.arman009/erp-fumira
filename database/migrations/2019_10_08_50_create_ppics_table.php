<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePpicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ppics', function (Blueprint $table) {
            $table->increments('id', 10);
            $table->integer('marketing_id')->length(10)->unsigned();
            $table->string('wo_number', 15)->unique();
            $table->string('wo_type', 10);
            $table->date('wo_date');
            $table->integer('user_id')->length(10)->unsigned();
            $table->string('status', 20);
            $table->integer('qty_spk')->length(10)->nullable();
            $table->timestamps();

            $table->foreign('marketing_id')->references('id')->on('marketings')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('user_id')->references('id')->on('users')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ppics');
    }
}
