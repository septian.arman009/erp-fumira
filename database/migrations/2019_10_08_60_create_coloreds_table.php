<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColoredsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coloreds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('colored_pcn', 30);
            $table->integer('color_id')->length(10)->unsigned();
            $table->integer('inc_detail_id')->length(10)->unsigned();
            $table->float('length');
            $table->string('status', 20);
            $table->timestamps();

            $table->foreign('inc_detail_id')->references('id')->on('inc_details')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('color_id')->references('id')->on('colors')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coloreds');
    }
}
