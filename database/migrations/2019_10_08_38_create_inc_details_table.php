<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inc_details', function (Blueprint $table) {
            $table->increments('id', 10);
            $table->integer('incoming_id')->length(10)->unsigned();
            $table->string('raw_pcn', 30)->unique();
            $table->integer('raw_id')->length(10)->unsigned();
            $table->float('length');
            $table->string('status', 20);
            $table->timestamps();

            $table->foreign('incoming_id')->references('id')->on('incomings')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('raw_id')->references('id')->on('raws')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inc_details');
    }
}
