<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id', 10);
            $table->string('product_type', 50)->unique();
            $table->string('type', 30);
            $table->string('cap', 30);
            $table->float('thick');
            $table->integer('width')->length(10);
            $table->integer('length')->length(10);
            $table->integer('color_id')->length(10)->unsigned()->nullable();
            $table->integer('user_id')->length(10)->unsigned();
            $table->float('gramatur');
            $table->timestamps();

            $table->foreign('color_id')->references('id')->on('colors')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('user_id')->references('id')->on('users')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
