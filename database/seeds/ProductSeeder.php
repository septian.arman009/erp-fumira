<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\Group;
use App\Color;
use App\User;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product_01 = new Product();
        $product_01->type = 'Forming';
        $product_01->cap = 'Fcc';
        $product_01->thick = 0.2;
        $product_01->width = 762;
        $product_01->length = 2000;
        $product_01->gramatur = 1.29;
        $product_01->color_id = Color::all()->random()->id;
        $product_01->user_id = User::all()->random()->id;
        $product_01->product_type = 'Forming/0.2/762/2000/'. $product_01->color_id;
        $product_01->save();

        $product_02 = new Product();
        $product_02->type = 'Corugation';
        $product_02->cap = 'Cs';
        $product_02->thick = 0.6;
        $product_02->width = 914;
        $product_02->length = 2000;
        $product_02->gramatur = 4.48;
        $product_02->color_id = Color::all()->random()->id;
        $product_02->user_id = User::all()->random()->id;
        $product_02->product_type = 'Corrugation/0.6/914/2000/'. $product_02->color_id;
        $product_02->save();

        $product_03 = new Product();
        $product_03->type = 'Corugation';
        $product_03->cap = 'Cs';
        $product_03->thick = 0.6;
        $product_03->width = 914;
        $product_03->length = 2000;
        $product_03->gramatur = 4.48;
        $product_03->color_id = NULL;
        $product_03->user_id = User::all()->random()->id;
        $product_03->product_type = 'Corrugation/0.6/914/2000';
        $product_03->save();
    }
}
