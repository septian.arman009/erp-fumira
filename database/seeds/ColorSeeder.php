<?php

use Illuminate\Database\Seeder;
use App\Color;
use App\User;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $color_01 = new Color();
        $color_01->color_name = 'f.101/f.505';
        $color_01->color_code = 'GC';
        $color_01->user_id = User::all()->random()->id;
        $color_01->save();

        $color_02 = new Color();
        $color_02->color_name = 'f.203/f505';
        $color_02->color_code = 'GC';
        $color_02->user_id = User::all()->random()->id;
        $color_02->save();
    }
}
