<?php

use Illuminate\Database\Seeder;
use App\Incoming;
use App\User;
use App\IncDetail;

class InvSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $incoming = new Incoming();
        $incoming->inc_number = 'INC/00001';
        $incoming->user_id = User::all()->random()->id;
        $incoming->status = 'finish';
        $incoming->save();
        
        $inc_detail = new IncDetail();
        $inc_detail->Incoming_id = $incoming->id;
        $inc_detail->raw_pcn = 'A.001';
        $inc_detail->raw_id = 1;
        $inc_detail->length = 1200;
        $inc_detail->status = 'Ready';
        $inc_detail->save();

        $inc_detail_01 = new IncDetail();
        $inc_detail_01->Incoming_id = $incoming->id;
        $inc_detail_01->raw_pcn = 'A.002';
        $inc_detail_01->raw_id = 2;
        $inc_detail_01->length = 1200;
        $inc_detail_01->status = 'Ready';
        $inc_detail_01->save();
    }
}
