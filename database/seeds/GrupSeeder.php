<?php

use Illuminate\Database\Seeder;
use App\Group;
use App\User;

class GrupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $group_01 = new Group();
        $group_01->machine_name = 'Coloring Machine (CL3001)';
        $group_01->machine_code = 'CL3001';
        $group_01->user_id = User::all()->random()->id;
        $group_01->save();
    }
}
