<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Raw;

class RawSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $raw_01 = new Raw();
        $raw_01->raw_type = 'RM/0.20/762/Soft';
        $raw_01->spec = 'Soft';
        $raw_01->thick = 0.20;
        $raw_01->width = 762;
        $raw_01->user_id = User::all()->random()->id;
        $raw_01->save();

        $raw_02 = new Raw();
        $raw_02->raw_type = 'RM/0.35/914/Hard';
        $raw_02->spec = 'Hard';
        $raw_02->thick = 0.35;
        $raw_02->width = 914;
        $raw_02->user_id = User::all()->random()->id;
        $raw_02->save();

        $raw_03 = new Raw();
        $raw_03->raw_type = 'RM/0.60/914/Soft';
        $raw_03->spec = 'Soft';
        $raw_03->thick = 0.60;
        $raw_03->width = 914;
        $raw_03->user_id = User::all()->random()->id;
        $raw_03->save();

        $raw_04 = new Raw();
        $raw_04->raw_type = 'RM/0.20/914/Hard';
        $raw_04->spec = 'Hard';
        $raw_04->thick = 0.20;
        $raw_04->width = 914;
        $raw_04->user_id = User::all()->random()->id;
        $raw_04->save();
    }
}
