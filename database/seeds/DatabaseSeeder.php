<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(ColorSeeder::class);
        $this->call(RawSeeder::class);
        $this->call(GrupSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(MarketingSeeder::class);
        $this->call(InvSeeder::class);
        $this->call(PpicSeeder::class);
    }
}
