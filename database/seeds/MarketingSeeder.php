<?php

use Illuminate\Database\Seeder;
use App\Marketing;
use App\User;

class MarketingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $marketing_01 = new Marketing();
        $marketing_01->do_number = 'DO/00001';
        $marketing_01->product_id = 1;
        $marketing_01->qty = 100;
        $marketing_01->date_request = '2019-10-01';
        $marketing_01->date_send = '2019-10-31';
        $marketing_01->user_id = User::all()->random()->id;
        $marketing_01->save();
    }
}
