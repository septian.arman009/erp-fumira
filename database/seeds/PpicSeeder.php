<?php

use Illuminate\Database\Seeder;
use App\Ppic;
use App\User;
use App\SpkDetail;

class PpicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ppic_01 = new Ppic();
        $ppic_01->marketing_id = 1;
        $ppic_01->wo_number ='SPK/RAW/00001';
        $ppic_01->wo_type = 'Base';
        $ppic_01->wo_date = '2019-10-10';
        $ppic_01->user_id = User::all()->random()->id;
        $ppic_01->status = 'Waiting';
        $ppic_01->save();

        $spkdetail_01 = new SpkDetail();
        $spkdetail_01->ppic_id = $ppic_01->id;
        $spkdetail_01->inc_detail_id = 1;
        $spkdetail_01->save();
    }
}
