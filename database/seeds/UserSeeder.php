<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UserSeeder extends Seeder
{
    public function run()
    {
        $adminRole = new Role();
        $adminRole->name = 'admin';
        $adminRole->display_name = 'Admin';
        $adminRole->save();

        $admin = new User();
        $admin->name = 'Admin';
        $admin->email = 'admin@gmail.com';
        $admin->password = bcrypt('12345678');
        $admin->is_verified = 1;
        $admin->save();
        $admin->attachRole($adminRole);

        $adminRole_01 = new Role();
        $adminRole_01->name = 'marketing';
        $adminRole_01->display_name = 'Marketing';
        $adminRole_01->save();

        $adminRole_02 = new Role();
        $adminRole_02->name = 'ppic';
        $adminRole_02->display_name = 'Ppic';
        $adminRole_02->save();

        $adminRole_03 = new Role();
        $adminRole_03->name = 'operator';
        $adminRole_03->display_name = 'Operator';
        $adminRole_03->save();

        $adminRole_04 = new Role();
        $adminRole_04->name = 'gudang';
        $adminRole_04->display_name = 'Gudang';
        $adminRole_04->save();

        $marketing = new User();
        $marketing->name = 'Marketing';
        $marketing->email = 'marketing@gmail.com';
        $marketing->password = bcrypt('12345678');
        $marketing->is_verified = 1;
        $marketing->save();
        $marketing->attachRole($adminRole_01);

        $ppic = new User();
        $ppic->name = 'PPIC';
        $ppic->email = 'ppic@gmail.com';
        $ppic->password = bcrypt('12345678');
        $ppic->is_verified = 1;
        $ppic->save();
        $ppic->attachRole($adminRole_02);

        $operator = new User();
        $operator->name = 'Operator';
        $operator->email = 'operator@gmail.com';
        $operator->password = bcrypt('12345678');
        $operator->is_verified = 1;
        $operator->save();
        $operator->attachRole($adminRole_03);

        $gudang = new User();
        $gudang->name = 'Gudang';
        $gudang->email = 'gudang@gmail.com';
        $gudang->password = bcrypt('12345678');
        $gudang->is_verified = 1;
        $gudang->save();
        $gudang->attachRole($adminRole_04);

    }
}
