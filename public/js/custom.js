$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document.body).on('click', '.form-delete', function (e) {
        e.preventDefault();
        var $a = $(this);
        var message = $a.data('confirm') ? $a.data('confirm') : 'Are you sure want to do this ?';
        mConfirm(message);
        $("#yes-modal-confirm").on('click', function () {
            var url = $a.data('url');
            var type = 'delete';
            postData(url, type, data = {}, function (err, response) {
                if (response) {
                    if ($.isEmptyObject(response.error)) {
                        baris = $('#form-' + response.id).closest('tr');
                        baris.fadeOut(200, function () {
                            $a.remove();
                        });
                    } else {
                        mError(response.error);
                    }
                } else {
                    console.log('ini error : ', err);
                }
            });
        });
    });

    $(document.body).on('submit', '.form-save', function (e) {
        e.preventDefault();
        var $form = $(this);
        var data = $form.serialize();
        var type = $form.attr('method');
        var url = $form.attr('action');
        var rule = $form.data('rule');
        $(".form-group").removeClass("has-error");
        $("#btn-save").val("Loading..");
        postData(url, type, data, function (err, response) {
            if (response) {

                if ($.isEmptyObject(response.error)) {

                    if (rule == "store") {
                        $form.trigger("reset");

                    } else if (rule == "store-response") {
                        $form.trigger("reset");
                        fillSomething($form.data('response'), $form.data('target'));

                    } else if (rule == "store-response-incoming") {
                        $form.trigger("reset");
                        incDetail($form.data('inc_number'));

                    } else {
                        window.location = $form.data('redirect');
                    }

                    mSuccess(response.success);
                    $("#btn-save").val("Simpan");
                } else {
                    mErrorArray(response.error);
                    $("#btn-save").val("Simpan");
                }

            } else {
                console.log('ini error : ', err);
            }
        });
    });

});

function postData(url, type, data, callback) {
    $.ajax({
        url: url,
        type: type,
        dataType: "json",
        data: data,
        success: function (response) {
            return callback(null, response);
        },
        error: function (err) {
            return callback(true, err);
        }
    });
}

var modalConfirm = $('#btn-c');
var modalError = $('#btn-e');
var modalErrorArray = $('#btn-e-array');
var modalSuccess = $('#btn-s');

function mConfirm(message) {
    modalConfirm.click();
    $("#message-btn-c").html(message);
}

function mError(message) {
    modalError.click();
    $("#message-btn-e").html(message);
}

function mErrorArray(message) {
    modalErrorArray.click();
    $("#message-btn-e-array").find("ul").html('');
    $("#message-btn-e-array").css('display', 'block');
    $.each(message, function (key, value) {
        $(".group-" + key).addClass("has-error");
        $.each(value, function (k, val) {
            $("#message-btn-e-array").find("ul").append('<li>' + val + '</li>');
        });
    });
}

function mSuccess(message) {
    modalSuccess.click();
    $("#message-btn-s").html(message);
}

function resetModal() {
    $("#message-btn-c").html('');
    $("#message-btn-e").html('');
    $("#message-btn-s").html('');
}

function loadView(url, div) {
    $.ajax({
        url: url,
        success: function (data) {
            $(div).html(data);
        }
    });
}

function fillSomething(url, form) {
    $.ajax({
        url: url,
        success: function (data) {
            $(form).val(data);
        }
    });
}