<div class="form-group group-marketing_id">
    {!! Form::label('marketing_id', 'Nomor Devlivery Order', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::select('marketing_id',
        ['' => '- Pilih Nomor DO -']+App\Marketing::where('qty', '>', 0)->pluck('do_number', 'id')->all(),
        (empty($spk)) ? null : $spk->marketing_id, ['class' => 'form-control', 'id' => 'marketing_id']) !!}
    </div>
</div>

<div class="form-group group-wo_number">
    {!! Form::label('wo_number', 'Nomor SPK', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::text('wo_number', (empty($wo_number)) ? null : $wo_number,
        ['class' => 'form-control', 'style' => 'color:black;cursor:pointer', 'readonly']) !!}
    </div>
</div>

<div class="form-group group-wo_date">
    {!! Form::label('wo_date', 'Tanggal SPK', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::text('wo_date', null, ['class' => 'form-control wo-date', 'style' => 'color:black;cursor:pointer',
        'readonly']) !!}
    </div>
</div>

<div class="form-group group-qty_spk">
    {!! Form::label('qty_spk', 'Jumlah', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::number('qty_spk', null, ['class' => 'form-control']) !!}
    </div>
</div>

@section('scripts')
<script>
    $(document).ready(function () {
        $(".wo-date").datepicker({
            format: 'yyyy-mm-dd'
        });
    });

    $("#marketing_id").on("change", function () {
        var url = "{{ url('admin/ppic/colorings/do_detail') }}/" + $(this).val();
        postData(url, 'get', data = {}, function (err, response) {
            if (response) {
                $.each(response.data, function (key, value) {
                    $("#" + key).html(value);
                });
            } else {
                console.log('ini error : ', err);
            }
        });
    });
</script>
@endsection