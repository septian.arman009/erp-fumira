@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>PPIC</li>
    <li>SPK FGS</li>
    <li class="active">Edit</li>
</ul>


<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Form SPK</h3>
                </div>

                {!! Form::model($spk, ['url' => route('fgs.update', $spk->id),
                'class' => 'form-horizontal form-save',
                'method' =>'put',
                'data-redirect' => route('fgs.index')
                ]) !!}

                <div class="panel-body">
                    <a class="btn btn-default waves-effect" href="{{ route('fgs.index') }}">
                        <i class="fa fa-arrow-left"> Kembali</i>
                    </a>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            @include('admin.ppic.fgs._form')
                        </div>
                        <div class="col-md-6">
                            @include('admin.ppic.coloring._doDetail')
                        </div>
                    </div>
                </div>
                
                <div class="panel-footer">
                    {!! Form::submit('Simpan', ['class'=>'btn btn-primary pull-right', 'id' => 'btn-save']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection