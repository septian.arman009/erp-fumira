<div class="do-detail">

    <ul class="list-group">
        <li class="list-group-item d-flex justify-content-between align-items-center active">
            Detail Delivery Order (DO)
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Tipe Produk
            <span class="badge badge-primary badge-pill">
                <p id="product_type">{{ (empty($spk)) ? '...' : $spk->marketing->product->product_type }}</p>
            </span>
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Jenis
            <span class="badge badge-primary badge-pill">
                <p id="type">{{ (empty($spk)) ? '...' : $spk->marketing->product->type }}</p>
            </span>
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Stampel
            <span class="badge badge-primary badge-pill">
                <p id="cap">{{ (empty($spk)) ? '...' : $spk->marketing->product->cap }}</p>
            </span>
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Warna
            <span class="badge badge-primary badge-pill">
                <p id="color_name">
                    @php
                        if(!empty($spk)) {
                            if(!is_null($spk->marketing->product->color)){
                                echo $spk->marketing->product->color->color_name;
                            }else{
                                echo '...';
                            }
                        }else{
                            echo '...';
                        }
                    @endphp
                </p>
            </span>
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Tebal
            <span class="badge badge-primary badge-pill">
                <p id="thick">{{ (empty($spk)) ? '...' : $spk->marketing->product->thick }}</p>
            </span>
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Lebar
            <span class="badge badge-primary badge-pill">
                <p id="width">{{ (empty($spk)) ? '...' : $spk->marketing->product->width }}</p>
            </span>
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Panjang
            <span class="badge badge-primary badge-pill">
                <p id="length">{{ (empty($spk)) ? '...' : $spk->marketing->product->length }} mm</p>
            </span>
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Jumlah
            <span class="badge badge-primary badge-pill">
                <p id="qty">{{ (empty($spk)) ? '...' : $spk->marketing->qty }}</p>
            </span>
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Tanggal Permintaan
            <span class="badge badge-primary badge-pill">
                <p id="date_request">{{ (empty($spk)) ? '...' : $spk->marketing->date_request }}</p>
            </span>
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Tanggal Pengiriman
            <span class="badge badge-primary badge-pill">
                <p id="date_send">{{ (empty($spk)) ? '...' : $spk->marketing->date_send }}</p>
            </span>
        </li>
        <li style="display:none"></li>
    </ul>

</div>