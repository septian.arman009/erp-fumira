@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>PPIC</li>
    <li>SPK Pewarnaan Raw Material</li>
    <li class="active">Tambah Data</li>
</ul>


<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Form SPK</h3>
                </div>
                
                {!! Form::open(['url' => route('colorings.store'), 
                    'class' => 'form-horizontal form-save', 
                    'method' =>'post', 
                    'data-redirect' => url('admin/ppic/colorings'),
                    'data-rule' => 'update'
                ]) !!}

                <div class="panel-body">
                    <a class="btn btn-default waves-effect" href="{{ route('colorings.index') }}">
                        <i class="fa fa-arrow-left"> Kembali</i>
                    </a>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            @include('admin.ppic.coloring._form')
                            <hr>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>PCN</th>
                                        <th>Tipe Raw</th>
                                        <th>Tebal</th>
                                        <th>Lebar</th>
                                        <th>Panjang</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($stocks as $stock)
                                        @if (empty($stock->spkdetail))
                                        <tr>
                                            <td>
                                                <label class="switch">
                                                    {!! Form::checkbox('raw[]', $stock->id, 0, ['id' => 'raw-'.$loop->iteration]) !!}
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td>{{ $stock->raw_pcn }}</td>
                                            <td>{{ $stock->raw->raw_type }}</td>
                                            <td>{{ $stock->raw->thick }}</td>
                                            <td>{{ $stock->raw->width }}</td>
                                            <td>{{ $stock->length }}</td>
                                        </tr>
                                        @endif
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            @include('admin.ppic.coloring._doDetail')
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    {!! Form::submit('Simpan', ['class'=>'btn btn-primary pull-right', 'id' => 'btn-save']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection