@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>PPIC</li>
    <li class="active">SPK Potong Laminating</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Daftar SPK Potong Laminating</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <a class="btn btn-default waves-effect" href="{{ route('cuttings.create') }}">Tambah SPK</a>
                        <hr>
                        <div class="col-md-12">
                            <table id="ppics" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="10%">ID</th>
                                        <th>Nomor Delivery Order</th>
                                        <th>Nomor SPK</th>
                                        <th>Tanggal SPK</th>
                                        <th>Dibuat Oleh</th>
                                        <th>Status</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td>
                                            {!! Form::text('do_number', null, ['class' => 'form-control', 'placeholder' => 'Nomor DO', 'id' => 'do_number']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('wo_number', null, ['class' => 'form-control', 'placeholder' => 'Nomor SPK', 'id' => 'wo_number']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('wo_date', null, ['class' => 'form-control', 'placeholder' => 'Tanggal SPK', 'id' => 'wo_date']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('user', null, ['class' => 'form-control', 'placeholder' => 'Pengguna', 'id' => 'user']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('status', null, ['class' => 'form-control', 'placeholder' => 'Status', 'id' => 'status']) !!}
                                        </td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function () {
        var table = $('#ppics').DataTable({
            processing: false,
            serverSide: true,
            ajax: {
                url: "{{ route('ppic.table', 'Laminating') }}",
                data: function (d) {
                    d.do_number = $('#do_number').val(),
                    d.wo_number = $('#wo_number').val(),
                    d.wo_date = $('#wo_date').val(),
                    d.user = $('#user').val(),
                    d.status = $('#status').val()
                }
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'do_number',
                    name: 'do_number'
                },
                {
                    data: 'wo_number',
                    name: 'wo_number'
                },
                {
                    data: 'wo_date',
                    name: 'wo_date'
                },
                {
                    data: 'user',
                    name: 'user',
                },
                {
                    data: 'status',
                    name: 'status',
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });

        $('#do_number').on('change', function () {
            table.draw();
        });

        $('#wo_number').on('change', function () {
            table.draw();
        });

        $('#wo_date').on('change', function () {
            table.draw();
        });

        $('#user').on('change', function () {
            table.draw();
        });

        $('#status').on('change', function () {
            table.draw();
        });
    });
</script>
@endsection

@section('style')
<style>
    #ppics_filter {
        display: none;
    }
</style>
@endsection