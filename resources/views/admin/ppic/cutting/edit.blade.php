@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Produksi</li>
    <li>SPK Potong Fgs</li>
    <li class="active">Edit</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Form SPK</h3>
                </div>

                {!! Form::model($spk, ['url' => route('cuttings.update', $spk->id),
                'class' => 'form-horizontal form-save',
                'method' =>'put',
                'data-redirect' => route('cuttings.index')
                ]) !!}

                <div class="panel-body">
                    <a class="btn btn-default waves-effect" href="{{ route('cuttings.index') }}">
                        <i class="fa fa-arrow-left"> Kembali</i>
                    </a>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            @include('admin.ppic.coloring._form')
                            <hr>
                            <h3>NonColored Base Roll Material</h3>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>PCN</th>
                                        <th>Tipe Raw</th>
                                        <th>Tebal</th>
                                        <th>Lebar</th>
                                        <th>Panjang</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($noncolored as $non)
                                        @if(empty($non->spkdetail))
                                        <tr>
                                            <td>
                                                <label class="switch">
                                                    {!! Form::checkbox('noncolored[]', $non->id, 0, ['id' =>
                                                    'raw-'.$loop->iteration]) !!}
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td>{{ $non->raw_pcn }}</td>
                                            <td>{{ $non->raw->raw_type }}</td>
                                            <td>{{ $non->raw->thick }}</td>
                                            <td>{{ $non->raw->width }}</td>
                                            <td>{{ $non->length }}</td>
                                        </tr>
                                        @else
                                            @if ($non->spkdetail->ppic_id == $spk->id)
                                            <tr>
                                                <td>
                                                    <label class="switch">
                                                        {!! Form::checkbox('noncolored[]', $non->id, 1, ['id' =>
                                                        'raw-'.$loop->iteration]) !!}
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td>{{ $non->raw_pcn }}</td>
                                                <td>{{ $non->raw->raw_type }}</td>
                                                <td>{{ $non->raw->thick }}</td>
                                                <td>{{ $non->raw->width }}</td>
                                                <td>{{ $non->length }}</td>
                                            </tr>
                                            @endif
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                            <h3>Colored Base Roll Material</h3>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>PCN</th>
                                        <th>Warna</th>
                                        <th>Tipe Raw</th>
                                        <th>Tebal</th>
                                        <th>Lebar</th>
                                        <th>Panjang</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($colored as $color)
                                        @if (empty($color->spkcoloreddetail))
                                            @php $inc = App\IncDetail::where('id', $color->inc_detail_id)->first(); @endphp
                                            <tr>
                                                <td>
                                                    <label class="switch">
                                                        {!! Form::checkbox('color[]', $color->id, 0, ['id' =>
                                                        'raw-'.$loop->iteration]) !!}
                                                        <span></span>
                                                    </label>
                                                </td>

                                                <td>{{ $color->colored_pcn }}</td>
                                                <td>{{ $color->color->color_name }}</td>
                                                <td>{{ $inc->raw->raw_type }}</td>
                                                <td>{{ $inc->raw->thick }}</td>
                                                <td>{{ $inc->raw->width }}</td>
                                                <td>{{ $color->length }}</td>
                                            </tr>
                                        @else
                                            @if ($color->spkcoloreddetail->ppic_id == $spk->id)
                                                @php $inc = App\IncDetail::where('id', $color->inc_detail_id)->first(); @endphp
                                                <tr>
                                                    <td>
                                                        <label class="switch">
                                                            {!! Form::checkbox('color[]', $color->id, 1, ['id' =>
                                                            'raw-'.$loop->iteration]) !!}
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                    <td>{{ $color->colored_pcn }}</td>
                                                    <td>{{ $color->color->color_name }}</td>
                                                    <td>{{ $inc->raw->raw_type }}</td>
                                                    <td>{{ $inc->raw->thick }}</td>
                                                    <td>{{ $inc->raw->width }}</td>
                                                    <td>{{ $color->length }}</td>
                                                </tr>
                                            @endif
                                        @endif
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            @include('admin.ppic.coloring._doDetail')
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    {!! Form::submit('Simpan', ['class'=>'btn btn-primary pull-right', 'id' => 'btn-save']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection