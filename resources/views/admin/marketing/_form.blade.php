<div class="form-group group-do_number">
    {!! Form::label('do_number', 'Nomor Delivery Order (DO)', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::text('do_number', (empty($do_number)) ? null : $do_number, ['class' => 'form-control',
        'style' => 'color:black;cursor:pointer', 'readonly']) !!}
    </div>
</div>

<div class="form-group group-product_id">
    {!! Form::label('product_id', 'Tipe Produk', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::select('product_id', ['' => '- Pilih Tipe Product -']+App\Product::pluck('product_type', 'id')->all(),
        (empty($order)) ? null : $order->product_id, ['class' => 'form-control', 'id' => 'product_id']) !!}
    </div>
</div>

<div class="form-group group-qty">
    {!! Form::label('qty', 'Jumlah Produk', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::number('qty', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group group-date_request">
    {!! Form::label('date_request', 'Tanggal Permintaan', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::text('date_request', null, ['class' => 'form-control request-date', 'style' =>
        'color:black;cursor:pointer', 'readonly']) !!}
    </div>
</div>

<div class="form-group group-date_send">
    {!! Form::label('date_send', 'Tanggal Kirim', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::text('date_send', null, ['class' => 'form-control send-date', 'style' => 'color:black;cursor:pointer',
        'readonly']) !!}
    </div>
</div>

@section('scripts')
<script>
    $(document).ready(function () {
        $(".request-date").datepicker({
            format: 'yyyy-mm-dd'
        });
        $(".send-date").datepicker({
            format: 'yyyy-mm-dd'
        });
    });

    $("#product_id").on("change", function () {
        var url = "{{ url('admin/marketing/orders/product_detail') }}/" + $(this).val();
        postData(url, 'get', data = {}, function (err, response) {
            if (response) {
                $.each(response.data, function (key, value) {
                    $("#"+key).html(value);
                });
            } else {
                console.log('ini error : ', err);
            }
        });
    });
</script>
@endsection