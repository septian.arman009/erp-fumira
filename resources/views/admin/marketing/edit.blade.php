@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Master Data</li>
    <li>Delivery Order (DO)</li>
    <li class="active">Edit</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Form Delivery Order (DO)</h3>
                </div>
                {!! Form::model($order, ['url' => route('orders.update', $order->id), 'class' => 'form-horizontal
                form-save',
                'method' =>'put', 'data-redirect' => route('orders.index')]) !!}
                <div class="panel-body">
                    <a class="btn btn-default waves-effect" href="{{ route('orders.index') }}">
                        <i class="fa fa-arrow-left"> Kembali</i>
                    </a>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            @include('admin.marketing._form')
                        </div>
                        <div class="col-md-6">
                            @include('admin.marketing._productDetail')
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    {!! Form::submit('Simpan', ['class'=>'btn btn-primary pull-right', 'id' => 'btn-save']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection