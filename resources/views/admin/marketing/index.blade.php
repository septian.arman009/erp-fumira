@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Marketing</li>
    <li class="active">Delivery Order (DO)</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Daftar Delivery Order (DO)</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <a class="btn btn-default waves-effect" href="{{ route('orders.create') }}">Tambah DO</a>
                        <hr>
                        <div class="col-md-12">
                            <table id="orders" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="10%">ID</th>
                                        <th>Nomor Delivery Order</th>
                                        <th>Tipe Produk</th>
                                        <th>Jumlah</th>
                                        <th>Tanggal Permintaan</th>
                                        <th>Tanggal Pengiriman</th>
                                        <th>Dibuat Oleh</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td>
                                            {!! Form::text('do_number', null, ['class' => 'form-control', 'placeholder' => 'Nomor DO', 'id' => 'do_number']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('product_type', null, ['class' => 'form-control', 'placeholder' => 'Tipe Produk', 'id' => 'product_type']) !!}
                                        </td>
                                        <td>
                                            {!! Form::number('qty', null, ['class' => 'form-control', 'placeholder' => 'Jumlah', 'id' => 'qty']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('date_request', null, ['class' => 'form-control', 'placeholder' => 'Tgl Permintaan', 'id' => 'date_request']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('date_send', null, ['class' => 'form-control', 'placeholder' => 'Tgl Pengiriman', 'id' => 'date_send']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('user', null, ['class' => 'form-control', 'placeholder' => 'Pengguna', 'id' => 'user']) !!}
                                        </td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function () {
        var table = $('#orders').DataTable({
            processing: false,
            serverSide: true,
            ajax: {
                url: "{{ route('orders.index') }}",
                data: function (d) {
                    d.do_number = $('#do_number').val(),
                    d.product_type = $('#product_type').val(),
                    d.qty = $('#qty').val(),
                    d.date_request = $('#date_request').val(),
                    d.date_send = $('#date_send').val(),
                    d.user = $('#user').val()
                }
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'do_number',
                    name: 'do_number'
                },
                {
                    data: 'product_type',
                    name: 'product_type'
                },
                {
                    data: 'qty',
                    name: 'qty'
                },
                {
                    data: 'date_request',
                    name: 'date_request'
                },
                {
                    data: 'date_send',
                    name: 'date_send'
                },
                {
                    data: 'user',
                    name: 'user',
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });

        $('#do_number').on('change', function () {
            table.draw();
        });

        $('#product_type').on('change', function () {
            table.draw();
        });

        $('#qty').on('change', function () {
            table.draw();
        });

        $('#date_request').on('change', function () {
            table.draw();
        });
        
        $('#date_send').on('change', function () {
            table.draw();
        });

        $('#user').on('change', function () {
            table.draw();
        });
    });
</script>
@endsection

@section('style')
<style>
    #orders_filter {
        display: none;
    }
</style>
@endsection