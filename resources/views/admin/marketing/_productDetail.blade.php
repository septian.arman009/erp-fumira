<div class="product-detail">

    <ul class="list-group">
        <li class="list-group-item d-flex justify-content-between align-items-center active">
            Detail Produk
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Tipe Produk
            <span class="badge badge-primary badge-pill">
                <p id="product_type">{{ (empty($order)) ? '...' : $order->product->product_type }}</p>
            </span>
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Jenis
            <span class="badge badge-primary badge-pill"> 
                    <p id="type">{{ (empty($order)) ? '...' : $order->product->type }}</p>
            </span>
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Stampel
            <span class="badge badge-primary badge-pill">
                    <p id="cap">{{ (empty($order)) ? '...' : $order->product->cap }}</p>
            </span>
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Warna
            <span class="badge badge-primary badge-pill">
                @if(empty($order))
                    <p id="color_name">...</p>
                @else
                    <p id="color_name">{{ (empty($order->product->color_id)) ? 'Tanpa Warna' : $order->product->color->color_name }}</p>
                @endif
            </span>
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Tebal
            <span class="badge badge-primary badge-pill">
                <p id="thick">{{ (empty($order)) ? '...' : $order->product->thick }}</p>
            </span>
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Lebar
            <span class="badge badge-primary badge-pill">
                <p id="width">{{ (empty($order)) ? '...' : $order->product->width }}</p>
            </span>
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Panjang
            <span class="badge badge-primary badge-pill">
                <p id="length">{{ (empty($order)) ? '...' : $order->product->length }}</p>
            </span>
        </li>
        <li style="display:none"></li>
    </ul>

</div>