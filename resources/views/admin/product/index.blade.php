@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Master Data</li>
    <li class="active">Tipe Produk</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Daftar Tipe Produk</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <a class="btn btn-default waves-effect" href="{{ route('products.create') }}">Tambah Tipe Produk</a>
                        <hr>
                        <div class="col-md-12">
                            <table id="products" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="10%">ID</th>
                                        <th>Tipe Produk</th>
                                        <th>Jenis</th>
                                        <th>Stampel</th>
                                        <th>Tebal</th>
                                        <th>Lebar</th>
                                        <th>Panjang</th>
                                        <th>Warna</th>
                                        <th>Dibuat Oleh</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td>
                                            {!! Form::text('product_type', null, ['class' => 'form-control', 'placeholder' => 'Tipe Produk', 'id' => 'product_type']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('type', null, ['class' => 'form-control', 'placeholder' => 'Jenis', 'id' => 'type']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('cap', null, ['class' => 'form-control', 'placeholder' => 'Stampel', 'id' => 'cap']) !!}
                                        </td>
                                        <td>
                                            {!! Form::number('thick', null, ['class' => 'form-control', 'placeholder' => 'Tebal', 'id' => 'thick']) !!}
                                        </td>
                                        <td>
                                            {!! Form::number('width', null, ['class' => 'form-control', 'placeholder' => 'Lebar', 'id' => 'width']) !!}
                                        </td>
                                        <td>
                                            {!! Form::number('length', null, ['class' => 'form-control', 'placeholder' => 'Panjang', 'id' => 'length']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('color_name', null, ['class' => 'form-control', 'placeholder' => 'Warna', 'id' => 'color_name']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('user', null, ['class' => 'form-control', 'placeholder' => 'Pengguna', 'id' => 'user']) !!}
                                        </td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function () {
        var table = $('#products').DataTable({
            processing: false,
            serverSide: true,
            ajax: {
                url: "{{ route('products.index') }}",
                data: function (d) {
                    d.product_type = $('#product_type').val(),
                    d.type = $('#type').val(),
                    d.cap = $('#cap').val(),
                    d.thick = $('#thick').val(),
                    d.width = $('#width').val(),
                    d.length = $('#length').val(),
                    d.color_name = $('#color_name').val(),
                    d.user = $('#user').val()
                }
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'product_type',
                    name: 'product_type'
                },
                {
                    data: 'type',
                    name: 'type'
                },
                {
                    data: 'cap',
                    name: 'cap'
                },
                {
                    data: 'thick',
                    name: 'thick'
                },
                {
                    data: 'width',
                    name: 'width'
                },
                {
                    data: 'length',
                    name: 'length'
                },
                {
                    data: 'color_name',
                    name: 'color_name'
                },
                {
                    data: 'user',
                    name: 'user',
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });

        $('#product_type').on('change', function () {
            table.draw();
        });

        $('#type').on('change', function () {
            table.draw();
        });

        $('#cap').on('change', function () {
            table.draw();
        });
        
        $('#thick').on('change', function () {
            table.draw();
        });

        $('#width').on('change', function () {
            table.draw();
        });

        $('#length').on('change', function () {
            table.draw();
        });

        $('#color_name').on('change', function () {
            table.draw();
        });

        $('#user').on('change', function () {
            table.draw();
        });
    });
</script>
@endsection

@section('style')
<style>
    #products_filter {
        display: none;
    }
</style>
@endsection