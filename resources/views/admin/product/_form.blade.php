<div class="form-group group-product_type">
    {!! Form::label('product_type', 'Tipe Produk', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::text('product_type', null, ['class' => 'form-control', 'style' => 'color:black;cursor:pointer',
        'readonly', 'id' => 'product_type']) !!}
    </div>
</div>

<div class="form-group group-type">
    {!! Form::label('type', 'Jenis', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::select('type', [
            '' => '- Pilih Jenis Produk -',
            'Coil' => 'Coil',
            'Pelas' => 'Pelas',
            'Roll' => 'Roll',
            'Corrugation' => 'Corrugation',
            'Forming' => 'Forming'
        ],
        (empty($product)) ? null : $product->type, ['class' => 'form-control', 'onchange' => 'generate_code()']) !!}
    </div>
</div>

<div class="form-group group-cap">
    {!! Form::label('cap', 'Stampel', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::select('cap', ['' => '- Cap -','Fcc' => 'Fcc', 'Cs' => 'Cs', 'Fg' => 'Fg'],
        (empty($product)) ? null : $product->cap, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group group-color_id">
    {!! Form::label('color_id', 'Warna', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::select('color_id', ['' => '- Pilih Warna -']+App\Color::pluck('color_name', 'id')->all(),
        (empty($product)) ? null : $product->color_id, ['class' => 'form-control', 'onchange' => 'generate_code()']) !!}
    </div>
</div>

<div class="form-group group-thick">
    {!! Form::label('thick', 'Tebal', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::number('thick', null, ['class' => 'form-control', 'step' => '0.01', 'id' => 'thick',
        'onkeyup' => 'generate_code()']) !!}
    </div>
</div>

<div class="form-group group-width">
    {!! Form::label('width', 'Lebar', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::number('width', null, ['class' => 'form-control', 'id' => 'width', 'onkeyup' => 'generate_code()']) !!}
    </div>
</div>

<div class="form-group group-length">
    {!! Form::label('length', 'Panjang', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::number('length', null, ['class' => 'form-control', 'id' => 'length', 'onkeyup' => 'generate_code()']) !!}
    </div>
</div>

<div class="form-group group-gramatur">
    {!! Form::label('gramatur', 'Gramatur', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::number('gramatur', null, ['class' => 'form-control']) !!}
    </div>
</div>

@section('scripts')
<script>
    function generate_code() {
        var type = $("#type").val();
        var thick = $("#thick").val();
        var width = $("#width").val();
        var length = $("#length").val();
        var color_id = $("#color_id").val();

        if(color_id != ''){
            $("#product_type").val(type+"/"+thick+"/"+width+"/"+length+"/"+color_id);
        }else{
            $("#product_type").val(type+"/"+thick+"/"+width+"/"+length);
        }
    }
</script>
@endsection