@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li class="active">Beranda</li>
</ul>

<div class="page-content-wrap">
    <div class="col-md-12">
        @include('admin.layouts._flash')
    </div>
    <div class="row row-bg">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Statistik</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <a class="tile tile-default">
                                <h2>{{ $not }} Kg</h2>
                                <p>Total <b style="color:red">NOT</b> Tahun <b>{{ $tahun }}</b></p>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a class="tile tile-default">
                                <h2>{{ $total }} Kg</h2>
                                <p>Total Produksi Tahun <b>{{ $tahun }}</b></p>
                            </a>
                        </div>
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <div class="form-group group-tahun">
                                <label for="tahun">Ganti Tahun</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                    <select id="tahun" class="form-control">
                                        @for ($i = 2019; $i <= 2099; $i++) <option
                                            {{ ($i == $tahun) ? "selected" : "" }} value="{{ $i }}">
                                            {{ $i }}</option>
                                            @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div id="kilo" style="height: 300px;"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
</div>

@endsection

@section('scripts')
<script>
    Morris.Bar({
        element: 'kilo',
        data: {!!json_encode($bar) !!},
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Hasil Produksi'],
        barRatio: 0.4,
        barColors: ['#5c7572'],
        xLabelAngle: 10,
        hideHover: 'auto',
        resize: true
    });

    $("#tahun").on("change", function () {
        window.location.href = "{{ url('admin/home') }}/"+$(this).val();
    });
</script>
@endsection