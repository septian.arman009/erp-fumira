<div class="form-group group-machine_name">
    {!! Form::label('machine_name', 'Nama Mesin', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::text('machine_name', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group group-machine_code">
    {!! Form::label('machine_code', 'Kode Mesin', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::text('machine_code', null, ['class' => 'form-control']) !!}
    </div>
</div>