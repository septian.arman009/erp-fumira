@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Master Data</li>
    <li class="active">Group Mesin</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Daftar Mesin</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <a class="btn btn-default waves-effect" href="{{ route('groups.create') }}">Tambah Mesin</a>
                        <hr>
                        <div class="col-md-12">
                            <table id="groups" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="10%">ID</th>
                                        <th>Nama Mesin</th>
                                        <th>Kode Mesin</th>
                                        <th>Dibuat Oleh</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td>
                                            {!! Form::text('machine_name', null, ['class' => 'form-control', 'placeholder' => 'Nama Mesin', 'id' => 'machine_name']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('machine_code', null, ['class' => 'form-control', 'placeholder' => 'Kode Mesin', 'id' => 'machine_code']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('user', null, ['class' => 'form-control', 'placeholder' => 'Pengguna', 'id' => 'user']) !!}
                                        </td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function () {
        var table = $('#groups').DataTable({
            processing: false,
            serverSide: true,
            ajax: {
                url: "{{ route('groups.index') }}",
                data: function (d) {
                    d.product_type = $('#machine_name').val(),
                    d.type = $('#machine_code').val(),
                    d.user = $('#user').val()
                }
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'machine_name',
                    name: 'machine_name'
                },
                {
                    data: 'machine_code',
                    name: 'machine_code'
                },
                {
                    data: 'user',
                    name: 'user',
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });

        $('#machine_name').on('change', function () {
            table.draw();
        });

        $('#machine_code').on('change', function () {
            table.draw();
        });

        $('#user').on('change', function () {
            table.draw();
        });
    });
</script>
@endsection

@section('style')
<style>
    #groups_filter {
        display: none;
    }
</style>
@endsection