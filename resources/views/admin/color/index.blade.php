@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Master Data</li>
    <li class="active">Tipe Warna</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Daftar Tipe Warna</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <a class="btn btn-default waves-effect" href="{{ route('colors.create') }}">Tambah Tipe Warna</a>
                        <hr>
                        <div class="col-md-12">
                            <table id="colors" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="10%">ID</th>
                                        <th>Tipe Warna</th>
                                        <th>Kode Warna</th>
                                        <th>Bibuat Oleh</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td>
                                            {!! Form::text('color_name', null, ['class' => 'form-control', 'placeholder' => 'Tipe Warna', 'id' => 'color_name']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('color_code', null, ['class' => 'form-control', 'placeholder' => 'Kode Warna', 'id' => 'color_code']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('user', null, ['class' => 'form-control', 'placeholder' => 'Pengguna', 'id' => 'user']) !!}
                                        </td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function () {
        var table = $('#colors').DataTable({
            processing: false,
            serverSide: true,
            ajax: {
                url: "{{ route('colors.index') }}",
                data: function (d) {
                    d.name = $('#color_name').val(),
                    d.user = $('#user').val()
                }
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'color_name',
                    name: 'color_name'
                },
                {
                    data: 'color_code',
                    name: 'color_code'
                },
                {
                    data: 'user',
                    name: 'user',
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });

        $('#color_name').on('change', function () {
            table.draw();
        });

        $('#color_code').on('change', function () {
            table.draw();
        });

        $('#user').on('change', function () {
            table.draw();
        });
    });
</script>
@endsection

@section('style')
<style>
    #colors_filter {
        display: none;
    }
</style>
@endsection