<div class="form-group group-color_name">
    {!! Form::label('color_name', 'Nama Warna', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::text('color_name', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group group-color_code">
    {!! Form::label('color_code', 'Kode Warna', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::text('color_code', null, ['class' => 'form-control']) !!}
    </div>
</div>