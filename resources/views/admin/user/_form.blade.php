<div class="form-group group-name">
    {!! Form::label('name', 'Nama', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">
                <span class="fa fa-user"></span>
            </span>
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group group-email">
    {!! Form::label('email', 'Alamat E-mail', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">
                <span class="fa fa-envelope"></span>
            </span>
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

@if (last(request()->segments()) != 'edit')
<div class="form-group group-password">
    {!! Form::label('password', 'Password', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">
                <span class="fa fa-lock"></span>
            </span>
            {!! Form::password('password', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('password_confirmation', 'Konfirmasi Password', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">
                <span class="fa fa-lock"></span>
            </span>
            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
@endif

@if(!empty($user))
<div class="form-group">
    {!! Form::label('role_id', 'Pilih Role', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">
                <span class="fa fa-key"></span>
            </span>
            {!! Form::select('role_id', App\Role::pluck('display_name', 'id')->all(), $user->roles, ['class' =>'form-control']) !!}
        </div>
    </div>
</div>
@else
<div class="form-group">
    {!! Form::label('role_id', 'Pilih Role', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">
                <span class="fa fa-key"></span>
            </span>
            {!! Form::select('role_id', App\Role::pluck('display_name', 'id')->all(), null, ['class' =>'form-control']) !!}
        </div>
    </div>
</div>
@endif
