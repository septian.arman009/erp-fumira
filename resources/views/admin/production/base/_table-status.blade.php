<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Nomor DO</th>
            <th>Nomor SPK</th>
            <th>Raw Material</th>
            <th>PCN</th>
            <th>Panjang (m)</th>
            <th>Status</th>
            <th>Tanggal Proses (SPK)</th>
            <th>Tanggal Kirim (DO)</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($spk as $wo)
                @if ($wo->spkdetail->count() > 1)
                    @foreach ($wo->spkdetail as $spk)
                        @php $incdetail = App\IncDetail::where('id', $spk->inc_detail_id)->first() @endphp
                        @if ($loop->iteration == 1) 
                            <tr>
                                <td rowspan="{{ $wo->spkdetail->count() }}">{{ $wo->marketing->do_number }}</td>
                                <td rowspan="{{ $wo->spkdetail->count() }}">{{ $wo->wo_number }}</td>
                                <td>{{ $incdetail->raw->raw_type }}</td>
                                <td>{{ $incdetail->raw_pcn }}</td>
                                <td>{{ $incdetail->length}}</td>

                                @if ($incdetail->status == 'Ready')
                                    <td style="background-color: limegreen; color:white">{{ $incdetail->status}}</td>
                                @elseif($incdetail->status == 'On Proccess')
                                    <td style="background-color: orange; color:white">{{ $incdetail->status}}</td>
                                @elseif($incdetail->status == 'Rest Of')
                                    <td style="background-color: blue; color:white">
                                        {{ $incdetail->status }}
                                    </td>
                                @elseif($incdetail->status == 'Closed')
                                    <td style="background-color: #ccc; color:white">
                                        {{ $incdetail->status }}
                                    </td>
                                @endif

                                <td rowspan="{{ $wo->spkdetail->count() }}">{{ $wo->wo_date }}</td>
                                <td rowspan="{{ $wo->spkdetail->count() }}">{{ $wo->marketing->date_send }}</td>
                                
                                @if ($wo->status == 'Waiting')
                                    <td rowspan="{{ $wo->spkdetail->count() }}" style="background-color: limegreen; color:white">
                                        {{ $wo->status }}
                                    </td>
                                @elseif($wo->status == 'On Proccess')
                                    <td rowspan="{{ $wo->spkdetail->count() }}" style="background-color: orange; color:white">
                                        {{ $wo->status }}
                                    </td>
                                @elseif($wo->status == 'Closed')
                                    <td rowspan="{{ $wo->spkdetail->count() }}" style="background-color: #ccc; color:white">
                                        {{ $wo->status }}
                                    </td>
                                @endif
                            </tr>
                        @else
                        <tr>
                            <td>{{ $incdetail->raw->raw_type }}</td>
                            <td>{{ $incdetail->raw_pcn }}</td>
                            <td>{{ $incdetail->length}}</td>
                            @if ($incdetail->status == 'Ready')
                                <td style="background-color: limegreen; color:white">
                                    {{ $incdetail->status }}
                                </td>
                            @elseif($incdetail->status == 'On Proccess')
                                <td style="background-color: orange; color:white">
                                    {{ $incdetail->status }}
                                </td>
                            @elseif($incdetail->status == 'Rest Of')
                                <td style="background-color: blue; color:white">
                                    {{ $incdetail->status }}
                                </td>
                            @elseif($incdetail->status == 'Closed')
                                <td style="background-color: #ccc; color:white">
                                    {{ $incdetail->status }}
                                </td>
                            @endif
                        </tr>
                        @endif
                    @endforeach
                @else
                    @if ($wo->spkdetail->count())
                        @php $incdetail = App\IncDetail::where('id', $wo->spkdetail[0]->inc_detail_id)->first() @endphp
                        <tr>
                            <td>{{ $wo->marketing->do_number }}</td>
                            <td>{{ $wo->wo_number }}</td>
                            <td>{{ $incdetail->raw->raw_type }}</td>
                            <td>{{ $incdetail->raw_pcn }}</td>
                            <td>{{ $incdetail->length}}</td>

                            @if ($incdetail->status == 'Ready')
                                <td style="background-color: limegreen; color:white">{{ $incdetail->status}}</td>
                            @elseif($incdetail->status == 'On Proccess')
                                <td style="background-color: orange; color:white">{{ $incdetail->status}}</td>
                            @elseif($incdetail->status == 'Rest Of')
                                <td style="background-color: blue; color:white">
                                    {{ $incdetail->status }}
                                </td>
                            @elseif($incdetail->status == 'Closed')
                                <td style="background-color: #ccc; color:white">
                                    {{ $incdetail->status }}
                                </td>
                            @endif

                            <td>{{ $wo->wo_date }}</td>
                            <td>{{ $wo->marketing->date_send }}</td>

                            @if ($wo->status == 'Waiting')
                                <td style="background-color: limegreen; color:white">
                                    {{ $wo->status }}
                                </td>
                            @elseif($wo->status == 'On Proccess')
                                <td style="background-color: orange; color:white">
                                    {{ $wo->status }}
                                </td>
                            @elseif($wo->status == 'Closed')
                                <td style="background-color: #ccc; color:white">
                                    {{ $wo->status }}
                                </td>
                            @endif
                        </tr>
                    @endif
                @endif
        @endforeach
    </tbody>
</table>

