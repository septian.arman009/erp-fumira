<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading ui-draggable-handle">
            <h3 class="panel-title">{{ $onproccess->spkdetail->ppic->wo_number }} <small></small></h3>
        </div>
        <div class="panel-body list-group">
            <a class="list-group-item">Nomor Raw Material
                <span class="badge badge-default">{{ $onproccess->raw_pcn }}</span>
            </a>

            <a class="list-group-item">Tipe Raw <span
                    class="badge badge-default">{{ $onproccess->raw->raw_type }}</span>
            </a>

            <a class="list-group-item">Tebal <span class="badge badge-default">{{ $onproccess->raw->thick }}</span>
            </a>

            <a class="list-group-item">Lebar <span class="badge badge-default">{{ $onproccess->raw->width }}</span>
            </a>

            <a class="list-group-item">Panjang <span class="badge badge-default">{{ $onproccess->length }}</span>
            </a>
        </div>
    </div>
</div>

<div class="col-md-6">
    {!! Form::open(['url' => url('admin/production/base/raw_colored_store'),
    'class' => 'form-horizontal form-save',
    'method' =>'post',
    'data-rule' => 'update',
    'data-redirect' => url("admin/production/base")
    ]) !!}

    {!! Form::text('inc_detail_id', $onproccess->id, ['style' => 'display:none']) !!}
    {!! Form::text('inc_length', $onproccess->length, ['style' => 'display:none']) !!}
    <div class="form-group group-color_id">
        {!! Form::label('color_id', 'Warna', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 col-xs-12">
            {!! Form::select('color_id', ['' => '- Pilih Warna -']+App\Color::pluck('color_name', 'id')->all(),
            null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group group-machine_code">
        {!! Form::label('machine_code', 'Kode Mesin', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 col-xs-12">
            {!! Form::select('machine_code', ['' => '- Kode Mesin -']+App\Group::pluck('machine_code', 'machine_code')->all(),null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group group-length">
        {!! Form::label('length', 'Panjang (m)', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 col-xs-12">
            {!! Form::number('length', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    {!! Form::submit('Selesai', ['class' => 'btn btn-primary pull-right', 'id' => 'btn-do']) !!}

    {!! Form::close() !!}
</div>