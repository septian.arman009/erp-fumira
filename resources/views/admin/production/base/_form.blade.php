{!! Form::open(['url' => url('admin/production/base/base_proccess'),
    'class' => 'form-horizontal form-save',
    'method' =>'post',
    'data-rule' => 'update',
    'data-redirect' => url("admin/production/base")
]) !!}

<div class="form-group group-marketing_id">
    {!! Form::label('marketing_id', 'Nomor Delivery Order', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::select('marketing_id', ['null' => '- Pilih Nomor Do -']+App\Marketing::pluck('do_number', 'id')->all(),
        null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group group-ppic_id">
    {!! Form::label('ppic_id', 'Nomor SPK', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        <div id="wo-list">
            <select name="" id="" class="form-control"></select>
        </div>
    </div>
</div>

<div class="form-group group-inc_detail_id">
    {!! Form::label('inc_detail_id', 'Nomor PCN', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        <div id="raw-list">
            <select name="" id="" class="form-control"></select>
        </div>
    </div>
</div>

{!! Form::submit('Kerjakan', ['class' => 'btn btn-success pull-right', 'id' => 'btn-do']) !!}

{!! Form::close() !!}