@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Produksi</li>
    <li class="active">Proses Pewarnaan Base Material</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Proses Pewarnaan Raw Material</h3>
                </div>

                <div class="panel-body">
                    <div class="row">

                        @if (!empty($onproccess))
                        @include('admin.production.base._proccess')
                        @else
                        <div class="col-md-6">
                            @include('admin.production.base._form')
                        </div>
                        @endif
                    </div>

                    <hr>
                    <div class="table-status"></div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function () {
        loadView('{{ url("admin/production/base/table_status") }}', '.table-status');
    });

    $("#marketing_id").on("change", function () {
        loadView('{{ url("admin/production/base/do_spk_list") }}/' + $(this).val(), '#wo-list');
        setTimeout(() => {
            loadView('{{ url("admin/production/base/do_raw_list") }}/' + $("#ppic_id").val(),
                '#raw-list');
        }, 500);
    });

    function doraw() {
        loadView('{{ url("admin/production/base/do_raw_list") }}/' + $("#ppic_id").val(), '#raw-list');
    }
</script>
@endsection