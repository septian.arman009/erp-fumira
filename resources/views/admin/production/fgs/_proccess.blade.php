<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading ui-draggable-handle">
            <h3 class="panel-title">{{ $proccess->wo_number }} <small></small></h3>
        </div>

        <div class="panel-body list-group">
            <a class="list-group-item">Nomor Delivery Order
                <span class="badge badge-default">{{ $proccess->marketing->do_number }}</span>
            </a>
            <a class="list-group-item">Tipe Produk
                <span class="badge badge-default">{{ $proccess->marketing->product->product_type }}</span>
            </a>
            <a class="list-group-item">Warna
                <span
                    class="badge badge-default">{{ ($proccess->marketing->product->color) ? $proccess->marketing->product->color->color_name : 'Tanpa Warna' }}</span>
            </a>
            <a class="list-group-item">Tebal
                <span class="badge badge-default">{{ $proccess->marketing->product->thick }}</span>
            </a>
            <a class="list-group-item">Lebar
                <span class="badge badge-default">{{ $proccess->marketing->product->width }}</span>
            </a>
            <a class="list-group-item">Panjang (mm)
                <span class="badge badge-default">{{ $proccess->marketing->product->length }} mm</span>
            </a>
            <a class="list-group-item">Jumlah Pesanan
                <span class="badge badge-default">{{ $proccess->marketing->qty }}</span>
            </a>
            <a class="list-group-item">Jumlah SPK
                <span class="badge badge-default">{{ $proccess->qty_spk }}</span>
            </a>
        </div>
    </div>
</div>

<div class="col-md-6">
    {!! Form::open(['url' => url('admin/production/fgs/fgs_store'),
    'class' => 'form-horizontal form-save',
    'method' =>'post',
    'data-rule' => 'update',
    'btn-id' => 'btn-do',
    'data-redirect' => url("admin/production/fgs")
    ]) !!}

    {!! Form::number('laminating_id', null, ['style' => 'display:none', 'id' => 'laminating_id'])!!}
    {!! Form::number('marketing_id', $proccess->marketing_id, ['style' => 'display:none'])!!}
    {!! Form::number('ppic_id', $proccess->id, ['style' => 'display:none'])!!}

    <div class="form-group group-lot">
        {!! Form::label('lot', 'Nomor Lot Laminating', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 col-xs-12">
            {!! Form::text('lot', null, ['class' => 'form-control', 'placeholder' => 'No Lot', 'id' => 'lot', 'readonly']) !!}
        </div>
    </div>

    <div class="form-group group-class">
        {!! Form::label('class', 'Kelas', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 col-xs-12">
            {!! Form::select('class',[
                '1' => '1',
                '2' => '2'], 
            null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group group-description">
        {!! Form::label('description', 'Keterangan', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 col-xs-12">
            {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Deskripsi', 'autofocus']) !!}
        </div>
    </div>

    <div class="form-group group-pcn">
        {!! Form::label('pcn', 'Kode PCN', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 col-xs-12">
            {!! Form::select('pcn',[
                'G.680' => 'G.680',
                'F.714' => 'F.714', 
                'A.737' => 'A.737', 
                'CH4' => 'CH4'], 
            null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group group-status">
        {!! Form::label('status', 'Status Barang', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 col-xs-12">
            {!! Form::select('status',[
                'OK' => 'OK',
                'NOT' => 'NOT'], 
            null, ['class' => 'form-control']) !!}
        </div>
    </div>

    {!! Form::submit('Kerjakan', ['class' => 'btn btn-success pull-right', 'id' => 'btn-save']) !!}
    {!! Form::close() !!}

    {!! Form::open(['url' => url('admin/production/fgs/fgs_store_finish'),
    'class' => 'form-horizontal form-save',
    'method' =>'post',
    'data-rule' => 'update',
    'btn-id' => 'btn-do',
    'data-redirect' => url("admin/production/fgs")
    ]) !!}
    {!! Form::number('ppic_id', $proccess->id, ['style' => 'display:none'])!!}
    {!! Form::submit('Selesai', ['class' => 'btn btn-primary pull-right', 'id' => 'btn-finish']) !!}
    {!! Form::close() !!}
</div>

<div class="col-md-12">
    <hr>
    <h3>Material Laminating Yang Dapat Digunakan</h3>
    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>No Lot Laminating</th>
                <th>Tipe</th>
                <th>Panjang</th>
                <th>Status</th>
                <th width="10%">Ambil No Lot</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($laminatings as $laminating)
            <tr>
                <td>{{ $laminating->pcn.''.lot($laminating->lot) }}</td>
                <td>{{ $laminating->marketing->product->product_type }}</td>
                <td>{{ $laminating->length }} mm</td>
                <td>{{ $laminating->status }}</td>
                <td>
                    @php
                    $pcn = $laminating->pcn.''.lot($laminating->lot);
                    @endphp
                    <a class="btn btn-info" onclick="takeLot('{{$laminating->id}}', '{{$pcn}}')">Proses</a>
                </td>
            </tr>

            @endforeach

        </tbody>
    </table>
</div>

<style>
    #lot {
        color: black;
        cursor: pointer;
    }
</style>