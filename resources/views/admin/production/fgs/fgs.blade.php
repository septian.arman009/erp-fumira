@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Produksi</li>
    <li class="active">Proses FGS</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Proses FGS</h3>
                </div>

                <div class="panel-body">
                    <div class="row">
                        @if ($proccess)
                            @include('admin.production.fgs._proccess')
                        @else
                        <div class="col-md-6">
                            @include('admin.production.fgs._form')
                        </div>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#marketing_id").on("change", function () {
        if($(this).val() != 'null'){
            loadView('{{ url("admin/production/fgs/do_spk_list") }}/' + $(this).val(), '#wo-list');
        }
    });

    function takeLot(laminating_id, lot) {
        $("#lot").val(lot);
        $("#laminating_id").val(laminating_id);
    }
</script>
@endsection