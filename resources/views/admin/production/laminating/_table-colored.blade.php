<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Nomor DO</th>
            <th>Nomor SPK</th>
            <th>Raw Type</th>
            <th>PCN</th>
            <th>Warna</th>
            <th>Panjang (m)</th>
            <th>Status</th>
            <th>Tanggal Proses (SPK)</th>
            <th>Tanggal Kirim (DO)</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($spk as $wo)
                @if ($wo->spkcoloreddetail->count() > 1)
                    @foreach ($wo->spkcoloreddetail as $spk)
                        @php $incdetail = App\IncDetail::where('id', $spk->colored->inc_detail_id)->first(); @endphp

                        @if ($loop->iteration == 1) 
                            <tr>
                                <td rowspan="{{ $wo->spkcoloreddetail->count() }}">{{ $wo->marketing->do_number }}</td>
                                <td rowspan="{{ $wo->spkcoloreddetail->count() }}">{{ $wo->wo_number }}</td>
                                <td>{{ $incdetail->raw->raw_type }}</td>
                                <td>{{ $spk->colored->colored_pcn }}</td>
                                <td>{{ $spk->colored->color->color_name }}</td>
                                <td>{{ $spk->colored->length}} m</td>
                                
                                @if ($spk->colored->status == 'Ready')
                                    <td style="background-color: limegreen; color:white">{{ $spk->colored->status}}</td>
                                @elseif($spk->colored->status == 'On Proccess')
                                    <td style="background-color: orange; color:white">{{ $spk->colored->status}}</td>
                                @elseif($spk->colored->status == 'Closed')
                                    <td style="background-color: #ccc; color:white">
                                        {{ $spk->colored->status }}
                                    </td>
                                @endif

                                <td rowspan="{{ $wo->spkcoloreddetail->count() }}">{{ $wo->wo_date }}</td>
                                <td rowspan="{{ $wo->spkcoloreddetail->count() }}">{{ $wo->marketing->date_send }}</td>
                                
                                @if ($wo->status == 'Waiting')
                                    <td rowspan="{{ $wo->spkcoloreddetail->count() }}" style="background-color: limegreen; color:white">
                                        {{ $wo->status }}
                                    </td>
                                @elseif($wo->status == 'On Proccess')
                                    <td rowspan="{{ $wo->spkcoloreddetail->count() }}" style="background-color: orange; color:white">
                                        {{ $wo->status }}
                                    </td>
                                @elseif($wo->status == 'Closed')
                                    <td rowspan="{{ $wo->spkcoloreddetail->count() }}" style="background-color: #ccc; color:white">
                                        {{ $wo->status }}
                                    </td>
                                @endif
                            </tr>
                        @else
                        <tr>
                            <td>{{ $incdetail->raw->raw_type }}</td>
                            <td>{{ $spk->colored->colored_pcn }}</td>
                            <td>{{ $spk->colored->color->color_name }}</td>
                            <td>{{ $spk->colored->length}} m</td>
                            @if ($spk->colored->status == 'Ready')
                                <td style="background-color: limegreen; color:white">
                                    {{ $spk->colored->status }}
                                </td>
                            @elseif($spk->colored->status == 'On Proccess')
                                <td style="background-color: orange; color:white">
                                    {{ $spk->colored->status }}
                                </td>
                            @elseif($spk->colored->status == 'Waiting')
                                <td style="background-color: blue; color:white">
                                    {{ $spk->colored->status }}
                                </td>
                            @elseif($spk->colored->status == 'Closed')
                                <td style="background-color: #ccc; color:white">
                                    {{ $spk->colored->status }}
                                </td>
                            @endif
                        </tr>
                        @endif
                    @endforeach
                @else
                    @if ($wo->spkcoloreddetail->count())
                        @php $incdetail = App\IncDetail::where('id', $wo->spkcoloreddetail[0]->colored->inc_detail_id)->first(); @endphp
                        <tr>
                            <td>{{ $wo->marketing->do_number }}</td>
                            <td>{{ $wo->wo_number }}</td>
                            <td>{{ $incdetail->raw->raw_type }}</td>
                            <td>{{ $wo->spkcoloreddetail[0]->colored->colored_pcn }}</td>
                            <td>{{ $wo->spkcoloreddetail[0]->colored->color->color_name }}</td>
                            <td>{{ $wo->spkcoloreddetail[0]->colored->length}} m</td>

                            @if ($wo->spkcoloreddetail[0]->colored->status == 'Ready')
                                <td style="background-color: limegreen; color:white">{{ $wo->spkcoloreddetail[0]->colored->status}}</td>
                            @elseif($wo->spkcoloreddetail[0]->colored->status == 'On Proccess')
                                <td style="background-color: orange; color:white">{{ $wo->spkcoloreddetail[0]->colored->status}}</td>
                            @elseif($wo->spkcoloreddetail[0]->colored->status == 'Closed')
                                <td style="background-color: #ccc; color:white">
                                    {{ $wo->spkcoloreddetail[0]->colored->status }}
                                </td>
                            @endif

                            <td>{{ $wo->wo_date }}</td>
                            <td>{{ $wo->marketing->date_send }}</td>

                            @if ($wo->status == 'Waiting')
                                <td style="background-color: limegreen; color:white">
                                    {{ $wo->status }}
                                </td>
                            @elseif($wo->status == 'On Proccess')
                                <td style="background-color: orange; color:white">
                                    {{ $wo->status }}
                                </td>
                            @elseif($wo->status == 'Closed')
                                <td style="background-color: #ccc; color:white">
                                    {{ $wo->status }}
                                </td>
                            @endif
                        </tr>
                    @endif
                @endif
        @endforeach
    </tbody>
</table>

