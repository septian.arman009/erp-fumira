@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Produksi</li>
    <li class="active">Proses Potong Laminating</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Proses Potong laminating</h3>
                </div>

                <div class="panel-body">
                    <div class="row">

                        @if (!empty($noncolored) || !empty($colored))
                            @if (!empty($colored))
                                @include('admin.production.laminating._colored-proccess')
                            @else
                                @include('admin.production.laminating._non-proccess')
                            @endif
                        @else
                        <div class="col-md-6">
                            @include('admin.production.laminating._form')
                        </div>
                        @endif
                    </div>

                    <hr>
                    <h3>Non Color SPK</h3>
                    <div class="table-non"></div>
                    <hr>
                    <h3>Colored SPK</h3>
                    <div class="table-colored"></div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function () {
        loadView('{{ url("admin/production/laminating/table_non") }}', '.table-non');
        loadView('{{ url("admin/production/laminating/table_colored") }}', '.table-colored');
    });

    $("#marketing_id").on("change", function () {
        if($(this).val() != 'null'){
            loadView('{{ url("admin/production/laminating/do_spk_list") }}/' + $(this).val(), '#wo-list');
            setTimeout(() => {
                if($("#ppic_id").val() != 'null'){
                    $("#color_type").removeAttr('disabled');
                }
            }, 250);
        }else{
            $("#color_type").attr('disabled', 'disabled');
        }
        
    });

    function doraw() {
        loadView('{{ url("admin/production/laminating/do_raw_list") }}/' + $("#color_type").val() + '/' + $('#ppic_id').val(), '#raw-list');
    }
</script>
@endsection