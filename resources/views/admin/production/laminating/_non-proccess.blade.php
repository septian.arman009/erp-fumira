<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading ui-draggable-handle">
            <h3 class="panel-title">{{ $noncolored->spkdetail->ppic->wo_number }} <small></small></h3>
        </div>
        <div class="panel-body list-group">
            <a class="list-group-item">Nomor Raw Material
                <span class="badge badge-default">{{ $noncolored->raw_pcn }}</span>
            </a>

            <a class="list-group-item">Tipe Raw <span
                    class="badge badge-default">{{ $noncolored->raw->raw_type }}</span>
            </a>

            <a class="list-group-item">Tipe Produk <span
                class="badge badge-default">{{ $noncolored->spkdetail->ppic->marketing->product->product_type }}</span>
            </a>

            <a class="list-group-item">Jenis <span
                    class="badge badge-default">{{ $noncolored->spkdetail->ppic->marketing->product->type }}</span>
            </a>

            <a class="list-group-item">Cap <span
                    class="badge badge-default">{{ $noncolored->spkdetail->ppic->marketing->product->cap }}</span>
            </a>

            <a class="list-group-item">Tebal <span class="badge badge-default">{{ $noncolored->raw->thick }}</span>
            </a>

            <a class="list-group-item">Lebar <span class="badge badge-default">{{ $noncolored->raw->width }}</span>
            </a>

            <a class="list-group-item">Panjang <span class="badge badge-default">{{ $noncolored->spkdetail->ppic->marketing->product->length }}</span>
            </a>
        </div>
    </div>
</div>

<div class="col-md-6">
    {!! Form::open(['url' => url('admin/production/laminating/laminating_noncolored_store'),
    'class' => 'form-horizontal form-save',
    'method' =>'post',
    'data-rule' => 'update',
    'data-redirect' => url("admin/production/laminating")
    ]) !!}

    {!! Form::text('inc_detail_id', $noncolored->id, ['style' => 'display:none']) !!}
    {!! Form::text('inc_length', $noncolored->length, ['style' => 'display:none']) !!}

    <div class="form-group group-length">
        {!! Form::label('length', 'Panjang', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 col-xs-12">
            {!! Form::number('length', $noncolored->spkdetail->ppic->marketing->product->length, ['class' => 'form-control', 'autofocus']) !!}
        </div>
    </div>

    <div class="form-group group-description">
        {!! Form::label('description', 'Keterangan', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 col-xs-12">
            {!! Form::text('description', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group group->status">
        {!! Form::label('status', 'Status', ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9 col-xs-12">
            {!! Form::select('status', [
                'OK' => 'OK',
                'NOT' => 'NOT'
            ] ,null, ['class' => 'form-control']) !!}
        </div>
    </div>

    {!! Form::submit('Kerjakan', ['class' => 'btn btn-success pull-right', 'id' => 'btn-do']) !!}
    {!! Form::close() !!}

    {!! Form::open(['url' => url('admin/production/laminating/laminating_noncolored_store_finish'),
    'class' => 'form-horizontal form-save',
    'method' =>'post',
    'data-rule' => 'update',
    'data-redirect' => url("admin/production/laminating")
    ]) !!}
    {!! Form::text('inc_detail_id', $noncolored->id, ['style' => 'display:none']) !!}
    {!! Form::submit('Selesai', ['class' => 'btn btn-primary pull-right', 'id' => 'btn-do']) !!}
    {!! Form::close() !!}
</div>

<style>
    #readonly {
        color: black;
        cursor: pointer;
    }
</style>