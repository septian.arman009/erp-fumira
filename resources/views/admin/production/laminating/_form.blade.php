{!! Form::open(['url' => url('admin/production/laminating/laminating_proccess'),
'class' => 'form-horizontal form-save',
'method' =>'post',
'data-rule' => 'update',
'data-redirect' => url("admin/production/laminating")
]) !!}

<div class="form-group group-marketing_id">
    {!! Form::label('marketing_id', 'Nomor Delivery Order', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::select('marketing_id', ['' => '- Pilih Nomor Do -']+App\Marketing::pluck('do_number',
        'id')->all(),
        null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group group-ppic_id">
    {!! Form::label('ppic_id', 'Nomor SPK', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        <div id="wo-list">
            <select name="" id="" class="form-control"></select>
        </div>
    </div>
</div>

<div class="form-group group-color" id="color">
    {!! Form::label('color_type', 'Jenis Warna', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::select('color_type', ['' => '- Pilih Jenis -','NonColored' => 'NonColored', 'Colored' => 'Colored'],
        null, ['class' => 'form-control', 'onchange' => 'doraw()', 'id' => 'color_type', 'disabled']) !!}
    </div>
</div>

<div class="form-group group-raw_id" id="raw">
    {!! Form::label('raw_id', 'Nomor PCN', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        <div id="raw-list">
            <select name="" id="" class="form-control"></select>
        </div>
    </div>
</div>

{!! Form::submit('Kerjakan', ['class' => 'btn btn-success pull-right', 'id' => 'btn-do']) !!}

{!! Form::close() !!}