@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Inventory</li>
    <li class="active">Stock Raw Material</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Daftar Stock Raw Material</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="stocks" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="10%">ID</th>
                                        <th>Nomor Incoming</th>
                                        <th>Product Code Number</th>
                                        <th>Tipe Produk</th>
                                        <th>Tebal</th>
                                        <th>Lebar</th>
                                        <th>Panjang (m)</th>
                                        <th>Bibuat Oleh</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td>
                                            {!! Form::text('inc_number', null, ['class' => 'form-control', 'placeholder' => 'Nomor Incoming', 'id' => 'inc_number']) !!}
                                        </td>
                                        <td>
                                            {!! Form::number('raw_pcn', null, ['class' => 'form-control', 'placeholder' => 'Nomor Produk', 'id' => 'raw_pcn']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('raw_type', null, ['class' => 'form-control', 'placeholder' => 'Tipe', 'id' => 'raw_type']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('thick', null, ['class' => 'form-control', 'placeholder' => 'Tebal', 'id' => 'thick']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('width', null, ['class' => 'form-control', 'placeholder' => 'Lebar', 'id' => 'width']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('length', null, ['class' => 'form-control', 'placeholder' => 'Panjang', 'id' => 'length']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('user', null, ['class' => 'form-control', 'placeholder' => 'Pengguna', 'id' => 'user']) !!}
                                        </td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(function () {
        var table = $('#stocks').DataTable({
            processing: false,
            serverSide: true,
            ajax: {
                url: "{{ route('stocks.raw_material') }}",
                data: function (d) {
                    d.inc_number = $('#inc_number').val(),
                    d.raw_pcn = $('#raw_pcn').val(),
                    d.raw_type = $('#raw_type').val(),
                    d.thick = $('#thick').val(),
                    d.width = $('#width').val(),
                    d.length = $('#length').val(),
                    d.user = $('#user').val()
                }
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'inc_number',
                    name: 'inc_number'
                },
                {
                    data: 'raw_pcn',
                    name: 'raw_pcn'
                },
                {
                    data: 'raw_type',
                    name: 'raw_type'
                },
                {
                    data: 'thick',
                    name: 'thick'
                },
                {
                    data: 'width',
                    name: 'width'
                },
                {
                    data: 'length',
                    name: 'length'
                },
                {
                    data: 'user',
                    name: 'user'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });

        $('#inc_number').on('change', function () {
            table.draw();
        });

        $('#raw_pcn').on('change', function () {
            table.draw();
        });

        $('#raw_type').on('change', function () {
            table.draw();
        });

        $('#thick').on('change', function () {
            table.draw();
        });

        $('#width').on('change', function () {
            table.draw();
        });

        $('#length').on('change', function () {
            table.draw();
        });

        $('#user').on('change', function () {
            table.draw();
        });
    });
</script>
@endsection

@section('style')
<style>
    #stocks_filter {
        display: none;
    }
</style>
@endsection