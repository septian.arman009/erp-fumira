<div class="raw-detail">
    <ul class="list-group">
        <li class="list-group-item d-flex justify-content-between align-items-center active">
            Detail Raw Material
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Spesifikasi
            <span class="badge badge-primary badge-pill">
                <p id="spec">{{ (empty($stock)) ? '...' : $stock->raw->spec }}</p>
            </span>
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Tebal
            <span class="badge badge-primary badge-pill">
                <p id="thick">{{ (empty($stock)) ? '...' : $stock->raw->thick }}</p>
            </span>
        </li>

        <li class="list-group-item d-flex justify-content-between align-items-center">
            Lebar
            <span class="badge badge-primary badge-pill">
                <p id="width">{{ (empty($stock)) ? '...' : $stock->raw->width }}</p>
            </span>
        </li>
        <li style="display:none"></li>
    </ul>
</div>