<div class="form-group group-inc_number">
    {!! Form::label('inc_number', 'Nomor Incoming', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::text('inc_number', $inc_number, ['class' => 'form-control', 'readonly',
        'style' => 'color: black;cursor:pointer;']) !!}
    </div>
</div>

<div class="form-group group-raw_pcn">
    {!! Form::label('raw_pcn', 'Product Code Number', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::text('raw_pcn', null, ['class' => 'form-control mask-pcn']) !!}
    </div>
</div>

<div class="form-group group-raw_id">
    {!! Form::label('raw_id', 'Tipe Raw Material', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::select('raw_id', ['' => '- Pilih Tipe Raw Material -']+App\Raw::pluck('raw_type', 'id')->all(), null,
        ['class' => 'form-control', 'id' => 'raw_type'])
        !!}
    </div>
</div>

<div class="form-group group-length">
    {!! Form::label('length', 'Panjang Material (m)', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::number('length', null, ['class' => 'form-control']) !!}
    </div>
</div>


@section('scripts')
<script>
    $(document).ready(function(){
        incDetail('{{ $inc_number }}');
    });

    $("#raw_type").on("change", function () {
        var url = "{{ url('admin/inventory/incomings/raw_detail') }}/" + $(this).val();
        postData(url, 'get', data = {}, function (err, response) {
            if (response) {
                $.each(response.data, function (key, value) {
                    $("#"+key).html(value);
                });
            } else {
                console.log('ini error : ', err);
            }
        });
    });

    function incDetail(inc_number) {
        var data = {
            inc_number: inc_number
        }

        var url = "{{ url('admin/inventory/incomings/inc_detail') }}";
        postData(url, 'post', data, function (err, response) {
            if(response){
                
                $("#inc-detail").html(response.html);
                
            }else{
                console.log('ini error : ', err);
            }
        });
    }

    $("#btn-finish").on("click", function() {
        mConfirm('Apakah penerimaan barang sudah selesai ?');
        $("#yes-modal-confirm").on('click', function () {
            var data = {
                inc_number: $("#inc_number").val()
            }
            $("#btn-finish").html('Loading..');
            var url = "{{ url('admin/inventory/incomings/inc_finish') }}";
            postData(url, 'post', data, function (err, response) {
                if(response){
                    if(response.status == 'success'){
                        $(".form-save").trigger('reset');
                        fillSomething('{{ route("incomings.getId", "incomings") }}', '#inc_number');
                        incDetail(null);
                        $("#btn-finish").html('Selesai');
                    }else if(response.status == 'empty'){
                        setTimeout(() => {
                            mError('Tidak terdapat produk pada nomor incoming tersebut.');
                            $("#btn-finish").html('Selesai');
                        }, 1000);
                    }
                    
                }else{
                    console.log('ini error : ', err);
                }
            });
        });
    });

    $("#btn-edit-finish").on("click", function() {
        mConfirm('Apakah penerimaan barang sudah selesai ?');
        $("#yes-modal-confirm").on('click', function () {
            var data = {
                inc_number: $("#inc_number").val()
            }
            $("#btn-edit-finish").html('Loading..');
            var url = "{{ url('admin/inventory/incomings/inc_finish') }}";
            postData(url, 'post', data, function (err, response) {
                if(response){
                    if(response.status == 'success'){
                        window.location = '{{ url("admin/inventory/stocks/raw_material") }}';
                    }else if(response.status == 'empty'){
                        setTimeout(() => {
                            mError('Tidak terdapat produk pada nomor incoming tersebut.');
                            $("#btn-finish").html('Selesai');
                        }, 1000);
                    }
                }else{
                    console.log('ini error : ', err);
                }
            });
        });
    });

    function remove(id) {
        mConfirm('Yakin ingin hapus barang ini ?');
        $("#yes-modal-confirm").on('click', function () {
            var url = "{{ url('admin/inventory/stocks/remove') }}/"+id;
            postData(url, 'delete', data = {}, function (err, response) {
                if(response){
                    incDetail($("#inc_number").val());
                }else{
                    console.log('ini error : ', err);
                }
            });
        });
        
    }

    $(".mask-pcn").mask('*.***');
</script>
@endsection