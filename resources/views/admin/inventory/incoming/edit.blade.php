@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Transaksi</li>
    <li>Stock Raw Material</li>
    <li class="active">Edit</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Form Barang Masuk</h3>
                </div>

                {!! Form::open(['url' => route('incomings.store'), 'class' => 'form-horizontal form-save',
                'method' =>'post',
                'data-rule' => 'store-response-incoming',
                'data-inc_number' => $inc_number]) !!}

                <div class="panel-body">
                    <div class="row">
                        <a class="btn btn-default waves-effect" href="{{ url('admin/inventory/stocks/raw_material') }}">
                            <i class="fa fa-arrow-left"> Kembali</i>
                        </a>
                        <hr>
                        <div class="col-md-6">
                            @include('admin.inventory.incoming._form')
                        </div>
                        <div class="col-md-6">
                            @include('admin.inventory.incoming._rawDetail')
                        </div>
                    </div>
                    <hr>
                    @include('admin.inventory.incoming._table')
                </div>
                <div class="panel-footer">
                    {!! Form::submit('Simpan', ['class'=>'btn btn-success pull-right', 'id' => 'btn-save']) !!}
                    {!! Form::close() !!}
                    <a class="btn btn-primary" id="btn-edit-finish">Selesai</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection