<div class="form-group group-raw_type">
    {!! Form::label('raw_type', 'Tipe Raw Material', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::text('raw_type', null, ['class' => 'form-control', 'style' => 'color:black;cursor:pointer;', 'readonly']) !!}
    </div>
</div>

<div class="form-group group-spec">
    {!! Form::label('spec', 'Spesifikasi', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::select('spec', ['' => '- Pilih Spesifikasi -','Soft' => 'Soft', 'Hard' => 'Hard'], (empty($raw)) ? null : $raw->spec, 
        ['class' => 'form-control', 'onchange' => 'generate_code()']) !!}
    </div>
</div>

<div class="form-group group-thick">
    {!! Form::label('thick', 'Tebal', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::number('thick', null, ['class' => 'form-control', 'step' => '0.01', 'onkeyup' => 'generate_code()']) !!}
    </div>
</div>

<div class="form-group group-width">
    {!! Form::label('width', 'Lebar', ['class' => 'col-md-3 control-label']) !!}
    <div class="col-md-9 col-xs-12">
        {!! Form::number('width', null, ['class' => 'form-control', 'onkeyup' => 'generate_code()']) !!}
    </div>
</div>

@section('scripts')
<script>
    function generate_code() {
        var segment = "{{ $title }}";
        if (segment == 'Tambah Tipe Raw Material') {
            var spec = $("#spec").val();
            var thick = $("#thick").val();
            var width = $("#width").val();
            
            $("#raw_type").val("RM/"+thick+"/"+width+"/"+spec);
        }
    }
</script>
@endsection