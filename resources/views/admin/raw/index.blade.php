@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Master Data</li>
    <li class="active">Tipe Raw Material</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Daftar Tipe Raw Material</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <a class="btn btn-default waves-effect" href="{{ route('raws.create') }}">Tambah Tipe Raw Material</a>
                        <hr>
                        <div class="col-md-12">
                            <table id="raws" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="10%">ID</th>
                                        <th>Type Raw Material</th>
                                        <th>Spesifikasi</th>
                                        <th>Tebal</th>
                                        <th>Lebar</th>
                                        <th>Dibuat Oleh</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <td>
                                            {!! Form::text('raw_type', null, ['class' => 'form-control', 'placeholder' =>
                                            'Tipe Material', 'id' => 'raw_type']) !!}
                                        </td>
                                        <td>
                                            {!! Form::text('spec', null, ['class' => 'form-control', 'placeholder' =>
                                            'Spesifikasi', 'id' => 'spec']) !!}
                                        </td>
                                        <td>
                                            {!! Form::number('thick', null, ['class' => 'form-control', 'placeholder' =>
                                            'Tebal', 'id' => 'thick']) !!}
                                        </td>
                                        <td>
                                            {!! Form::number('width', null, ['class' => 'form-control', 'placeholder' =>
                                            'Lebar', 'id' => 'width']) !!}
                                        </td>
                                        <td>
                                            {!! Form::number('user', null, ['class' => 'form-control', 'placeholder' =>
                                            'Pengguna', 'id' => 'user']) !!}
                                        </td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script raw_type="text/javascript">
    $(function () {
        var table = $('#raws').DataTable({
            processing: false,
            serverSide: true,
            ajax: {
                url: "{{ route('raws.index') }}",
                data: function (d) {
                    d.raw_type = $('#raw_type').val(),
                    d.spec = $('#spec').val(),
                    d.thick = $('#thick').val(),
                    d.width = $('#width').val(),
                    d.user = $('#user').val()
                }
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'raw_type',
                    name: 'raw_type'
                },
                {
                    data: 'spec',
                    name: 'spec'
                },
                {
                    data: 'thick',
                    name: 'thick'
                },
                {
                    data: 'width',
                    name: 'width'
                },
                {
                    data: 'user',
                    name: 'user'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                }
            ]
        });

        $('#raw_type').on('change', function () {
            table.draw();
        });

        $('#spec').on('change', function () {
            table.draw();
        });

        $('#thick').on('change', function () {
            table.draw();
        });

        $('#width').on('change', function () {
            table.draw();
        });

        $('#user').on('change', function () {
            table.draw();
        });


    });
</script>
@endsection

@section('style')
<style>
    #raws_filter {
        display: none;
    }
</style>
@endsection