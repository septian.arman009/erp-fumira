@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li>Master Data</li>
    <li>Tipe Raw Material</li>
    <li class="active">Edit</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Daftar Tipe Raw Material</h3>
                </div>
                {!! Form::model($raw, ['url' => route('raws.update', $raw->id), 'class' => 'form-horizontal form-save',
                 'method' =>'put', 'data-redirect' => route('raws.index')]) !!}
                <div class="panel-body">
                    <a class="btn btn-default waves-effect" href="{{ route('raws.index') }}">
                        <i class="fa fa-arrow-left"> Kembali</i>
                    </a>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            @include('admin.raw._form')
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    {!! Form::submit('Simpan', ['class'=>'btn btn-primary pull-right', 'id' => 'btn-save']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection