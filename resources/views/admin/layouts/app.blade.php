<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>{{ $title }}</title>
    <meta http-equiv=" Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{ asset('icon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" id="theme" href="{{ asset('admin/css/theme-default.css') }}" />

</head>

<body>
    <div class="page-container page-navigation-top-fixed">
        <div class="page-sidebar page-sidebar-fixed scroll">

            <ul class="x-navigation">

                <li class="xn-logo">
                    <a>Simple Erp</a>
                    <a class="x-navigation-control"></a>
                </li>

                <li class="xn-profile">
                    <a class="profile-mini">
                        <img src="{{ asset('fumira.png') }}" alt="Avatar">
                    </a>
                    <div class="profile">
                        <div class="profile-image">
                            <img style="border:none" src="{{ asset('fumira.png') }}" alt="Avatar">
                        </div>
                        <div class="profile-data">
                            <div class="profile-data-name">{{ Auth::user()->name }}</div>
                            <div class="profile-data-title">{{ Auth::user()->email }}</div>
                        </div>
                        <div class="profile-controls">
                            <a href="{{ url('admin/home/'.date('Y')) }}" class="profile-control-left"><span
                                    class="fa fa-bar-chart-o"></span></a>
                            <a href="{{ route('change_password')}}" class="profile-control-right"><span
                                    class="fa fa-lock"></span></a>
                        </div>
                    </div>
                </li>

                <li class="xn-title">Versi 1.0.0</li>
                {!! Html::mainMenu('home', url('admin/home/'.(date('Y'))), 'fa fa-home', 'Beranda') !!}

                @role('admin')
                <li class="xn-openable pointer {{ set_active('master') }}">
                    <a>
                        <span class="fa fa-hdd-o"></span>
                        <span class="xn-text">Data Master</span>
                    </a>
                    <ul>
                        {!! Html::subMenu('users', url('admin/master/users'), 'fa fa-user', 'Pengguna') !!}
                        {!! Html::subMenu('groups', url('admin/master/groups'), 'fa fa-dot-circle-o', 'Group Mesin') !!}
                        {!! Html::subMenu('colors', url('admin/master/colors'), 'fa fa-dot-circle-o', 'Tipe Warna') !!}
                        {!! Html::subMenu('raws', url('admin/master/raws'), 'fa fa-dot-circle-o', 'Tipe Raw Material') !!}
                        {!! Html::subMenu('products', url('admin/master/products'), 'fa fa-dot-circle-o', 'Tipe Produk') !!}
                    </ul>
                </li>
                @endrole
                
                @role(['admin', 'marketing'])
                <li class="xn-openable pointer {{ set_active('marketing') }}">
                    <a>
                        <span class="fa fa-clipboard"></span>
                        <span class="xn-text">Marketing</span>
                    </a>
                    <ul>
                        {!! Html::subMenu('orders', url('admin/marketing/orders'), 'fa fa-file-text-o', 'Delivery Order (DO)') !!}
                    </ul>
                </li>
                @endrole

                @role(['admin', 'gudang', 'marketing'])
                <li class="xn-openable pointer {{ set_active('inventory') }}">
                    <a>
                        <span class="fa fa-briefcase"></span>
                        <span class="xn-text">Inventory</span>
                    </a>
                    <ul>
                        {!! Html::subMenu('incomings', url('admin/inventory/incomings/create'), 'fa fa-mail-reply', 'Barang Masuk') !!}
                        {!! Html::subMenu('stocks', url('admin/inventory/stocks/raw_material'), 'fa fa-retweet', 'Stock Raw Material') !!}
                    </ul>
                </li>
                @endrole

                @role(['admin', 'marketing', 'ppic'])
                <li class="xn-openable pointer {{ set_active('ppic') }}">
                    <a>
                        <span class="fa fa-file"></span>
                        <span class="xn-text">PPIC</span>
                    </a>
                    <ul>
                        {!! Html::subMenu('colorings', url('admin/ppic/colorings'), 'fa fa-file-text', 'SPK Pewarnaan Raw M.') !!}
                        {!! Html::subMenu('cuttings', url('admin/ppic/cuttings'), 'fa fa-file-text', 'SPK Potong Laminating') !!}
                        {!! Html::subMenu('fgs', url('admin/ppic/fgs'), 'fa fa-file-text', 'SPK FGS') !!}
                    </ul>
                </li>
                @endrole

                @role(['admin', 'operator'])
                <li class="xn-openable pointer {{ set_active('production') }}">
                    <a>
                        <span class="fa fa-legal"></span>
                        <span class="xn-text">Produksi</span>
                    </a>
                    <ul>
                        {!! Html::subMenu('base', url('admin/production/base'), 'fa fa-circle', 'Coloring Base Roll') !!}
                        {!! Html::subMenu('laminating', url('admin/production/laminating'), 'fa fa-circle', 'Potong Laminating') !!}
                        {!! Html::subMenu('fgs', url('admin/production/fgs'), 'fa fa-circle', 'Potong FGS') !!}
                    </ul>
                </li>
                @endrole

                @role(['admin', 'operator', 'marketing', 'ppic'])
                {!! Html::mainMenu('result', url('admin/result/'.date('Y').'/fgs'), 'fa fa-check-square-o', 'Hasil Produksi') !!}
                @endrole
            </ul>
        </div>

        <div class="page-content">
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <li class="xn-icon-button">
                    <a class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                </li>
                <li class="xn-icon-button pull-right">
                    <a class="mb-control pointer" data-box="#mb-signout">
                        <span class="fa fa-sign-out"></span>
                    </a>
                </li>
            </ul>
            @yield('content')
        </div>
        @include('admin.layouts._notif')
        @include('admin.layouts._modal')
    </div>

    <audio id="audio-alert" src="{{ asset('admin/audio/alert.mp3') }}" preload="auto"></audio>
    <audio id="audio-fail" src="{{ asset('admin/audio/fail.mp3') }}" preload="auto"></audio>

    <script type="text/javascript" src="{{ asset('admin/js/plugins/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/jquery/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/bootstrap/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}">
    </script>
    <script type='text/javascript' src="{{ asset('admin/js/plugins/maskedinput/jquery.maskedinput.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('admin/js/plugins/bootstrap/bootstrap-datepicker.js') }}"></script>
    <script type='text/javascript' src="{{ asset('admin/js/plugins/bootstrap/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/morris/raphael-min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/plugins/morris/morris.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('admin/js/plugins.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin/js/actions.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>

    @yield('style')
    @yield('scripts')
</body>

<style>
    .pointer {
        cursor: pointer;
    }

    .row-bg {
        background: #f5f5f5 url('{{ url("admin/img/bg.png") }}') no-repeat;
    }

    .x-navigation>li.xn-logo>a:first-child {
        font-size: 25px;
        border-bottom: 0px;
        color: #FFF;
        height: 50px;
        text-align: center;
        background: #5c7572;
    }

    .x-navigation li.active>a {
        background: #5c7572;
        color: #fff;
        -webkit-transition: all 200ms ease;
        -moz-transition: all 200ms ease;
        -ms-transition: all 200ms ease;
        -o-transition: all 200ms ease;
        transition: all 200ms ease;
    }
</style>

</html>