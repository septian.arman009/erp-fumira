@if (session()->has('flash_notification.message'))
<div class="alert alert-{{ session()->get('flash_notification.level') }}" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span
            class="sr-only">Close</span></button>
    {!! session()->get('flash_notification.message') !!}
</div>
@endif