<div class="row">
    <div class="col-md-12">
        <table id="results" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th width="10%">ID</th>
                    <th>Nomor Delivery Order</th>
                    <th>Tipe Produk</th>
                    <th width="10%">PCN</th>
                    <th>Panjang (mm)</th>
                    <th>Kelas Produk</th>
                    <th width="13%">Tipe Warna</th>
                    <th width="15%">Tanggal</th>
                    <th width="10%">Status</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td></td>
                    <td>
                        {!! Form::text('do_number', null, ['class' => 'form-control', 'placeholder' => 'Nomor DO', 'id'
                        => 'do_number']) !!}
                    </td>
                    <td>
                        {!! Form::select('product_type', [
                        '' => '- Jenis -',
                        'Coil' => 'Coil',
                        'Pelas' => 'Pelas',
                        'Roll' => 'Roll',
                        'Corrugation' => 'Corrugation',
                        'Forming' => 'Forming'
                        ], null, ['class' => 'form-control', 'id' => 'product_type']) !!}
                    </td>
                    <td>
                        {!! Form::select('pcn',[
                        '' => '- PCN -',
                        'G.680' => 'G.680',
                        'F.714' => 'F.714',
                        'A.737' => 'A.737',
                        'CH4' => 'CH4'],
                        null, ['class' => 'form-control', 'id' => 'pcn']) !!}
                    </td>
                    <td>
                        {!! Form::number('length', null, ['class' => 'form-control', 'placeholder' => 'Panjang', 'id' =>
                        'length']) !!}
                    </td>
                    <td>
                        {!! Form::text('class', null, ['class' => 'form-control', 'placeholder' => 'Kelas Kualitas',
                        'id' => 'class']) !!}
                    </td>
                    <td>
                        {!! Form::select('color_type', [
                        '' => '- Warna -',
                        'Colored' => 'Colored',
                        'Non Color' => 'Non Color'
                        ], null, ['class' => 'form-control', 'id' => 'color_type']) !!}
                    </td>
                    <td>
                        {!! Form::select('date', [
                        '' => '-Bulan-',
                        'Januari' => 'Januari',
                        'Februari' => 'Februari',
                        'Maret' => 'Maret',
                        'April' => 'April',
                        'Mei' => 'Mei',
                        'Juni' => 'Juni',
                        'Juli' => 'Juli',
                        'Agustus' => 'Agustus',
                        'September' => 'Septemmber',
                        'Oktober' => 'Oktober',
                        'November' => 'November',
                        'Desember' => 'Desember'
                        ], null, ['class' => 'form-control', 'id' => 'date']) !!}
                    </td>
                    <td>
                        {!! Form::select('status', [
                        '' => '- Status -',
                        'OK' => 'OK',
                        'NOT' => 'NOT'
                        ], null, ['class' => 'form-control', 'id' => 'status']) !!}
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

@section('scripts')
<script type="text/javascript">
    $(function () {
        var table = $('#results').DataTable({
            processing: false,
            serverSide: true,
            order: [
                [0, 'desc']
            ],
            ajax: {
                url: "{{ route('result.fgs', $tahun) }}",
                data: function (d) {
                    d.do_number = $('#do_number').val(),
                        d.product_type = $('#product_type').val(),
                        d.pcn = $('#pcn').val(),
                        d.length = $('#length').val(),
                        d.class = $('#class').val(),
                        d.color_type = $('#color_type').val(),
                        d.date = $('#date').val(),
                        d.status = $('#status').val()
                }
            },
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'do_number',
                    name: 'do_number'
                },
                {
                    data: 'product_type',
                    name: 'product_type'
                },
                {
                    data: 'pcn',
                    name: 'pcn'
                },
                {
                    data: 'length',
                    name: 'length'
                },
                {
                    data: 'class',
                    name: 'class'
                },
                {
                    data: 'color_type',
                    name: 'color_type'
                },
                {
                    data: 'date',
                    name: 'date'
                },
                {
                    data: 'status',
                    name: 'status'
                }
            ]
        });

        $('#do_number').on('change', function () {
            table.draw();
        });
        $('#product_type').on('change', function () {
            table.draw();
        });
        $('#pcn').on('change', function () {
            table.draw();
        });
        $('#length').on('change', function () {
            table.draw();
        });
        $('#class').on('change', function () {
            table.draw();
        });
        $('#color_type').on('change', function () {
            table.draw();
        });
        $('#date').on('change', function () {
            table.draw();
        });
        $('#status').on('change', function () {
            table.draw();
        });

        $("#tahun").on("change", function () {
            let show_table = $("#table").val();
            window.location.href = "{{ url('admin/result') }}/" + $(this).val() + '/' + show_table;
        });

        $("#show_table").on("change", function () {
            let tahun = $("#tahun").val();
            window.location.href = "{{ url('admin/result') }}/" + tahun + '/' + $(this).val();
        });
    });
</script>
@endsection

@section('style')
<style>
    #results_filter {
        display: none;
    }
</style>
@endsection