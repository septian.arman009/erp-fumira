@extends('admin.layouts.app')

@section('content')
<ul class="breadcrumb">
    <li class="active">Hasil Produksi</li>
</ul>

<div class="page-content-wrap">
    <div class="row row-bg">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Daftar Hasil Produksi</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <div class="form-group group-tahun">
                                <label for="tahun">Data Produksi</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="fa fa-filter"></span>
                                    </span>
                                    <select id="show_table" class="form-control">
                                     @php
                                         $tables = ['fgs','laminating'];
                                         foreach ($tables as $tbl) {
                                             if($tbl == $table){
                                                echo "<option selected value='$tbl'>$tbl</option>";
                                             }else{
                                                echo "<option value='$tbl'>$tbl</option>";
                                             }
                                         }
                                     @endphp
                                    </select>
                                </div>
                            </div>

                            <div class="form-group group-tahun">
                                <label for="tahun">Ganti Tahun</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                    <select id="tahun" class="form-control">
                                        @for ($i = 2019; $i <= 2099; $i++) <option
                                            {{ ($i == $tahun) ? "selected" : "" }} value="{{ $i }}">
                                            {{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if ($table == 'fgs')
                    @include('admin.result.fgs')
                    @else
                    @include('admin.result.laminating')
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection