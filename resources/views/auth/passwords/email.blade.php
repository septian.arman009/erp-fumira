@extends('auth.layouts.app')

@section('content')
<div class="login-box animated fadeInDown">
    <div class="login-body">
        <div class="login-title"><strong>Forgot</strong> Password</div>
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif
        
        {!! Form::open(['url' => route('password.email'), 'class' => 'form-horizontal', 'method' => 'POST']) !!}
        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
            <div class="col-md-12">
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6">
                <a href="{{ route('login') }}" class="btn btn-link btn-block">Back To Sign In</a>
            </div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-primary btn-block">Send Link</button>
            </div>
        </div>
        {!! Form::close()!!}

    </div>
    <div class="login-footer">
        <div class="pull-left">
            &copy; 2019 Customer Relation
        </div>
        <div class="pull-right">
        </div>
    </div>
</div>
@endsection