@extends('admin.layouts.app')

@section('content')
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">

            <div class="error-container">
                <div class="error-code">404</div>
                <div class="error-text">Halaman tidak ditemukan</div>
                <div class="error-subtext">Sayang sekali halaman yang anda minta tidak kami temukan, silakan hubungi
                    admin untuk info selanjutnya.</div>
                <div class="error-actions">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{  url('admin/home') }}" class="btn btn-info btn-block btn-lg">Kembali ke
                                home</a>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ URL::previous() }}" class="btn btn-primary btn-block btn-lg">Halaman
                                sebelumnya</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

@endsection