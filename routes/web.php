<?php

/* Route For Main View */
Route::get('/', function () {
    return redirect('/login');
});

/* Route For Auth */
Auth::routes();

/* Route For Main Menu */
Route::group(
    [
        'prefix' => 'admin',
        'middleware' => ['auth', 'role:admin|marketing|operator|ppic|gudang'],
    ],
    function () {
        //Route Home
        Route::get('/home/{tahun}', 'admin\HomeController@index');
        
        //Route User
        Route::resource('master/users', 'Admin\UsersController');

        //Route Raw Material
        Route::resource('master/raws', 'Admin\RawsController');

        //Route Color Type
        Route::resource('master/colors', 'Admin\ColorsController');

        //Route Product Type
        Route::resource('master/products', 'Admin\ProductsController');

        //Route Machine Group
        Route::resource('master/groups', 'Admin\GroupsController');

        //Route Marketing (Delivery Order)
        Route::resource('marketing/orders', 'Admin\MarketingsController');
        Route::get('marketing/orders/get_id/{table}', [
            'as' => 'orders.getId',
            'uses' => 'Admin\MarketingsController@getId',
        ]);
        Route::get('marketing/orders/product_detail/{product_id}', 'Admin\MarketingsController@productDetail');

        //Route Change Password
        Route::get('change_password', [
            'as' => 'change_password',
            'uses' => 'Admin\UsersController@changePassword',
        ]);
        Route::post('store_password', [
            'as' => 'store_password',
            'uses' => 'Admin\UsersController@storeNewPassword',
        ]);

        //Route Incoming
        Route::get('inventory/incomings/create', [
            'as' => 'incomings.create',
            'uses' => 'Admin\InvController@incCreate',
        ]);
        Route::post('inventory/incomings/inc_detail', [
            'as' => 'incomings.inc_detail',
            'uses' => 'Admin\InvController@incDetail',
        ]);
        Route::post('inventory/incomings/inc_finish', [
            'as' => 'incomings.inc_finish',
            'uses' => 'Admin\InvController@incFinish',
        ]);
        Route::get('inventory/incomings/raw_detail/{id}', [
            'uses' => 'Admin\InvController@incRawDetail',
        ]);
        Route::post('inventory/incomings/store', [
            'as' => 'incomings.store',
            'uses' => 'Admin\InvController@incStore',
        ]);
        Route::get('inventory/incomings/get_id/{table}', [
            'as' => 'incomings.getId',
            'uses' => 'Admin\InvController@getId',
        ]);

        //Route Stock Raw Material
        Route::get('inventory/stocks/raw_material', [
            'as' => 'stocks.raw_material',
            'uses' => 'Admin\InvController@rawStock',
        ]);
        Route::get('inventory/stocks/{id}/edit', [
            'as' => 'stocks.edit',
            'uses' => 'Admin\InvController@editStock',
        ]);
        Route::put('inventory/stocks/update/{id}', [
            'as' => 'stocks.update',
            'uses' => 'Admin\InvController@updateStock',
        ]);
        Route::delete('inventory/stocks/{id}', [
            'as' => 'stocks.destroy',
            'uses' => 'Admin\InvController@destroyStock',
        ]);
        Route::delete('inventory/stocks/remove/{id}', [
            'as' => 'stocks.remove',
            'uses' => 'Admin\InvController@removeStock',
        ]);
        
        //Route PPIC SPK Base Material to Coloring Order
        Route::resource('ppic/colorings', 'Admin\PpicsController');
        Route::get('ppic/colorings/get_id/{table}', [
            'as' => 'colorings.getId',
            'uses' => 'Admin\PpicsController@getId',
        ]);
        Route::get('ppic/colorings/do_detail/{table}', 'Admin\PpicsController@doDetail');

        Route::get('ppic/table/{type}', [
            'as' => 'ppic.table',
            'uses' => 'Admin\PpicsController@table',
        ]);

        //Route PPIC SPK to Cutting finish good Roll
        Route::get('ppic/cuttings', [
            'as' => 'cuttings.index',
            'uses' => 'Admin\PpicsController@cuttingIndex',
        ]);
        Route::get('ppic/cuttings/create', [
            'as' => 'cuttings.create',
            'uses' => 'Admin\PpicsController@cuttingCreate',
        ]);
        Route::get('ppic/colorings/get_laminating_id/{table}', [
            'as' => 'colorings.getLaminatingId',
            'uses' => 'Admin\PpicsController@getLaminatingId',
        ]);
        Route::post('ppic/cuttings/store', [
            'as' => 'cuttings.store',
            'uses' => 'Admin\PpicsController@cuttingStore',
        ]);
        Route::get('ppic/cuttings/{cutting}/edit', [
            'as' => 'cuttings.edit',
            'uses' => 'Admin\PpicsController@cuttingEdit',
        ]);
        Route::put('ppic/cuttings/{cutting}', [
            'as' => 'cuttings.update',
            'uses' => 'Admin\PpicsController@cuttingUpdate',
        ]);
        Route::delete('ppic/cuttings/{cutting}', [
            'as' => 'cuttings.destroy',
            'uses' => 'Admin\PpicsController@cuttingDestroy',
        ]);

        //Route Production
        Route::get('production/base', [
            'as' => 'productions.base',
            'uses' => 'Admin\ProductionsController@baseColored',
        ]);
        Route::get('production/base/table_status', 'Admin\ProductionsController@tableStatus');
        Route::get('production/base/do_spk_list/{marketing_id}', 'Admin\ProductionsController@doSpk');
        Route::get('production/base/do_raw_list/{inc_detail_id}', 'Admin\ProductionsController@doRaw');
        Route::post('production/base/base_proccess', 'Admin\ProductionsController@baseProccess');
        Route::post('production/base/raw_colored_store', 'Admin\ProductionsController@rawColoredStore');
        
        Route::get('production/laminating/table_non', 'Admin\ProductionsController@tableStatusLaminatingNon');
        Route::get('production/laminating/table_colored', 'Admin\ProductionsController@tableStatusLaminatingColored');
        Route::post('production/laminating/laminating_proccess', 'Admin\ProductionsController@laminatingProccess');
        Route::get('production/laminating', 'Admin\ProductionsController@laminating');
        Route::get('production/laminating/do_spk_list/{marketing_id}', 'Admin\ProductionsController@doSpkLaminating');
        Route::get('production/laminating/do_raw_list/{color_type}/{ppic_id}', 'Admin\ProductionsController@doRawLaminating');

        Route::post('production/laminating/laminating_colored_store', 'Admin\ProductionsController@laminatingColoredStore');
        Route::post('production/laminating/laminating_colored_store_finish', 'Admin\ProductionsController@laminatingColoredStoreFinish');
        Route::post('production/laminating/laminating_noncolored_store', 'Admin\ProductionsController@laminatingNonColoredStore');
        Route::post('production/laminating/laminating_noncolored_store_finish', 'Admin\ProductionsController@laminatingNonColoredStoreFinish');
        
        Route::get('result/{tahun}/{table}', [
            'as' => 'result.index',
            'uses' => 'Admin\ResultController@index'
        ]);

        Route::get('result_fgs/{tahun}', [
            'as' => 'result.fgs',
            'uses' => 'Admin\ResultController@fgs'
        ]);

        Route::get('result_laminating/{tahun}', [
            'as' => 'result.laminating',
            'uses' => 'Admin\ResultController@laminating'
        ]);

        //SPK FGS
        Route::get('ppic/fgs', [
            'as' => 'fgs.index',
            'uses' => 'Admin\PpicsController@fgsIndex',
        ]);
        Route::get('ppic/fgs/create', [
            'as' => 'fgs.create',
            'uses' => 'Admin\PpicsController@fgsCreate',
        ]);
        Route::post('ppic/fgs/store', [
            'as' => 'fgs.store',
            'uses' => 'Admin\PpicsController@fgsStore',
        ]);
        Route::get('ppic/fgs/{fgs}/edit', [
            'as' => 'fgs.edit',
            'uses' => 'Admin\PpicsController@fgsEdit',
        ]);
        Route::put('ppic/fgs/{fgs}', [
            'as' => 'fgs.update',
            'uses' => 'Admin\PpicsController@fgsUpdate',
        ]);
        Route::delete('ppic/fgs/{fgs}', [
            'as' => 'fgs.destroy',
            'uses' => 'Admin\PpicsController@fgsDestroy',
        ]);

        Route::get('production/fgs', 'Admin\ProductionsController@fgs');

        Route::get('production/fgs/do_spk_list/{marketing_id}', 'Admin\ProductionsController@doSpkFgs');
        Route::post('production/fgs/fgs_proccess', 'Admin\ProductionsController@fgsProccess');
        Route::post('production/fgs/fgs_store', 'Admin\ProductionsController@fgsStore');
        Route::post('production/fgs/fgs_store_finish', 'Admin\ProductionsController@fgsStoreFinish');
    }
);

Route::get('auth/verify/{token}', 'Auth\RegisterController@verify');
Route::get('auth/send-verification', 'Auth\RegisterController@sendVerification');
